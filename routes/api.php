
<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/generateToken', 'TokenController@generateToken');

Route::post('/getProjectAccess', 'ProjectController@getProjectAccess');
Route::post('/saveProjectDetails', 'ProjectController@saveProjectDetails');
Route::post('/getProjectDetails', 'ProjectController@getProjectDetails');
Route::post('/searchProject', 'ProjectController@searchProject');
Route::post('/saveSiteDetails', 'ProjectController@saveSiteDetails');


Route::post('/getEmployees', 'EmployeeController@getEmployees');
Route::post('/getCompanies', 'EmployeeController@getCompanies');
Route::post('/getEmployeeById', 'EmployeeController@getEmployeesById');
Route::post('/getEmployeeProjects', 'EmployeeController@getEmployeeProjects');

Route::post('/getLogEntries', 'EmployeeController@getLogEntries');
Route::post('/saveLogEntries', 'EmployeeController@saveLogEntries');

Route::post('/importEmployees', 'EmployeeController@importEmployees');
Route::post('/updateEmployeeRole', 'EmployeeController@updateEmployeeRole');

Route::post('/getRoles', 'RolesController@getRoles');
Route::post('/getUserNotifications', 'UserNotificationController@getUserNotifications');
Route::post('/updateNotification', 'UserNotificationController@updateNotification');
Route::post('/login', 'LoginController@login');
Route::post('/changePassword', 'LoginController@changePassword');
Route::post('/forgotPassword', 'LoginController@forgotPassword');

Route::post('/getParamList', 'ParamController@getParamList');
Route::post('/getSelectedParamDetails', 'ParamController@getSelectedParamDetails');
Route::post('/getParamDetails', 'ParamController@getParamDetails');
Route::post('/saveParameter', 'ParamController@saveParameter');

Route::post('/doLogin', 'LoginController@login');

Route::post('/getAllCRM', 'ProjectController@getAllCRM');
Route::post('/saveCRM', 'ProjectController@saveCRM');

Route::post('/saveResource', 'EmployeeController@saveResource');