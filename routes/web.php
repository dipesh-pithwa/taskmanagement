<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
// Route::get('/', 'WelcomeController@welcome');
// Route::get('/(:any)/(:all?)', function () {
//     return view('welcome');
// });

Route::any('/provideAccess/{EmployeeId}/{ProjectId}', 'UIControllerOne@ProvideAccess');
Route::any('/send-emails', 'UIControllerOne@sendEmails');
Route::any('/login', 'UIControllerOne@login');
Route::any('/logout', 'UIControllerOne@logout');
Route::any('/forgot-password/{EmailId}/{Password}', 'UIControllerOne@ForgotPassword');
Route::any('/change-password/{EmailId}/{Password}', 'UIControllerOne@ForgotPassword');
Route::get('/{any}', 'WelcomeController@welcome')->where('any', '.*');
Route::get('/sess', 'SessionController@sess');
// Route::post('/doLogin', 'UIControllerOne@doLogin');
// Route::any('/doLogin2', function(){
// 	return "Hello";
// });
Route::any('/test', function(){
	return view('welcome');
});