<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use App\Models\UserNotification;
use Excel;
use Illuminate\Http\Request;

class UserNotificationController extends Controller {
    public function __construct(UserNotification $userNotification) {
        $this->UserNotification = $userNotification;
    }

    public function getUserNotifications(){
        $data = json_decode(file_get_contents('php://input'), true);
        $DataArr = $this->UserNotification->getUserNotifications($data);

        $UnReadCount = 0;
        foreach ($DataArr["response"] as $key => $value) {
            if(strtolower($value["ReadStatus"]) != "read"){
                $UnReadCount++;
            }
        }
        $TotalNotification = count($DataArr["response"]);
        if($DataArr["status"] == "success"){
            $responseArr = array(
                "code" => 200,
                "totalUnread" => $UnReadCount,
                "totalNotifications" => $TotalNotification,
                "notifications" => $DataArr["response"]
            );
        }else{
            $responseArr = array(
                "code" => 500,
                "totalUnread" => 0,
                "totalNotifications" => 0,
                "notifications" => []
            );
        }
        return response()->json($responseArr);
    }

    public function updateNotification(){
        $data = json_decode(file_get_contents('php://input'), true);
        $DataArr = $this->UserNotification->updateNotification($data);
        if($DataArr["status"] == "success"){
            $responseArr = array(
                "code" => 200,
                "message" => $DataArr["response"]
            );
        }else{
            $responseArr = array(
                "code" => 500,
                "message" => "Unable to Update."
            );
        }
        return response()->json($responseArr);
    }
}