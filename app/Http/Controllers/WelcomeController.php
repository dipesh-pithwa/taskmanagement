<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Redirect;
use Session,Lib,Config,DB,Lang,Request,Response;
class WelcomeController extends Controller{
	
	public function __construct()
	{

	}

	public function welcome(){
		Session::put('emailId', 'ishaan@uniply.in');
		Session::put('firstName', 'Vector');
		Session::put('role', 'Project Manager');
		Session::put('tenantId', '1');
		Session::put('employeeId', '1');
		Session::put('secret', 'OWM1NmNjNTFiMzc0YzNiYTE4OTIxMGQ1YjZkNGJmNTc3OTBkMzUxYzk2YzQ3YzAyMTkwZWNmMWU0MzA2MzVhYg==');
		Session::put('userId', '1');
		if(Session::get("emailId") == ""){
			return redirect('login');
		}else{
			$ViewData = [
				"emailId" => Session::get("emailId"),
				"firstName" => Session::get("firstName"),
				"role" => Session::get("role"),
				"tenantId" => Session::get("tenantId"),
				"employeeId" => Session::get("employeeId"),
				"secret" => Session::get("secret"),
				"userId" => Session::get("userId"),
			];
			return view('welcome', $ViewData);
		}
	}
}