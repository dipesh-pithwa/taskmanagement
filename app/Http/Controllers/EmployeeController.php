<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use App\Models\Employee;
use Excel;
use Illuminate\Http\Request;

class EmployeeController extends Controller {
    public function __construct(Employee $employee) {
        $this->Employee = $employee;
    }

    public function saveResource(){
        $data = json_decode(file_get_contents('php://input'), true);
        $DataArr = $this->Employee->saveResource($data);
        if($DataArr["status"] == "success"){
            $responseArr = array(
                "code" => 200,
                "result" => $DataArr["response"],
                "teamId" => $DataArr["teamId"]
            );
        }else{
            $responseArr = array(
                "code" => 500,
                "result" => [],
                "teamId" => ""
            );
        }
        return response()->json($responseArr);
    }

    public function getCompanies(){
        $data = json_decode(file_get_contents('php://input'), true);
        $DataArr = $this->Employee->getCompanies($data);
        if($DataArr["status"] == "success"){
            $responseArr = array(
                "code" => 200,
                "result" => $DataArr["response"]
            );
        }else{
            $responseArr = array(
                "code" => 500,
                "result" => []
            );
        }
        return response()->json($responseArr);
    }

    public function saveLogEntries(){
        $data = json_decode(file_get_contents('php://input'), true);
        $DataArr = $this->Employee->saveEmployeeLog($data);
        if($DataArr["status"] == "success"){
            $responseArr = array(
                "code" => 200,
                "result" => "logged time saved successfully"
            );
        }else{
            $responseArr = array(
                "code" => 500,
                "result" => []
            );
        }
        return response()->json($responseArr);
    }

    public function updateEmployeeRole(){
        $data = json_decode(file_get_contents('php://input'), true);
        $DataArr = $this->Employee->updateEmployeeRole($data);
        if($DataArr["status"] == "success"){
            $responseArr = array(
                "code" => 200,
                "response" => $DataArr["response"]
            );
        }else{
            $responseArr = array(
                "code" => 500,
                "response" => ""
            );
        }
        return response()->json($responseArr);
    }

    public function importEmployees(Request $request){
        $path = $request->file('importEmployees')->getRealPath();
        $data = Excel::load($path)->get();
        $postData = $request->all();
        $FailedRowNum = [];
        if($data->count() > 1){
            $RowNumber = 1;
            $ExcelDataCount = count($data);
            $ExcelRowCounter = 1;
            foreach ($data as $key => $value) {
                // if(($value["employeecode"] == "" || $value["firstname"] == "" || $value["lastname"] == "" || $value["companyname"] == "" || $value["emailaddress"] == "" || $value["employeecode"] == NULL || $value["firstname"] == NULL || $value["lastname"] == NULL || $value["companyname"] == NULL || $value["emailaddress"] == NULL) && $ExcelRowCounter != $ExcelDataCount){
                if(($value["employeecode"] == "" || $value["firstname"] == "" || $value["lastname"] == "" || $value["companyname"] == "" || $value["emailaddress"] == "" || $value["employeecode"] == NULL || $value["firstname"] == NULL || $value["lastname"] == NULL || $value["companyname"] == NULL || $value["emailaddress"] == NULL)){
                    $responseArr = array(
                        "code" => 200,
                        "totalRecordCount" => "",
                        "successRecordCount" => "",
                        "failedRecordCount" => "",
                        "failedRows" => [0 => ["RowNumber" => ""]]
                    );
                    return response()->json($responseArr);
                }
                $ExcelRowCounter++;
            }
            foreach ($data as $key => $value) {
                if($value["employeecode"] != "" && $value["firstname"] != "" && $value["lastname"] != "" && $value["companyname"] != "" && $value["emailaddress"] != ""){
                    $arr = [
                        "employeecode" => $value["employeecode"],
                        "firstname" => $value["firstname"],
                        "lastname" => $value["lastname"],
                        "salary" => $value["salary"],
                        "companyname" => $value["companyname"],
                        "emailaddress" => $value["emailaddress"],
                        "mobilenumber" => $value["mobilenumber"],
                        "department" => $value["department"],
                        "address" => $value["address"],
                        "city" => $value["city"],
                        "state" => $value["state"],
                        "country" => $value["country"],
                        "dateofjoining" => $value["dateofjoining"],
                        "role" => $value["role"],
                        "tenantId" => $postData["tenantId"]
                    ];
                    $ImportStatus = $this->Employee->importEmployees($arr);
                    if($ImportStatus["status"] != "success"){
                        $FailedRowNum[] = array("rowNumber" => $RowNumber, "reason" => $ImportStatus["response"], "data" => @$ImportStatus["data"]);
                    }
                    $RowNumber++;
                }
            }
        }
        $TotalRecords = $data->count();
        $responseArr = array(
            "code" => 200,
            "totalRecordCount" => $TotalRecords,
            "successRecordCount" => ($TotalRecords - count($FailedRowNum)),
            "failedRecordCount" => count($FailedRowNum),
            "failedRows" => $FailedRowNum
        );
        return response()->json($responseArr);
    }

    public function getLogEntries(){
        $data = json_decode(file_get_contents('php://input'), true);
        $DataArr = $this->Employee->getEmployeeLog($data);
        if($DataArr["status"] == "success"){
            $responseArr = array(
                "code" => 200,
                "logEntry" => $DataArr["response"]
            );
        }else{
            $responseArr = array(
                "code" => 500,
                "logEntry" => []
            );
        }
        return response()->json($responseArr);
    }

    public function getEmployees(){
        $data = json_decode(file_get_contents('php://input'), true);
        $DataArr = $this->Employee->getEmployee($data);
        if($DataArr["status"] == "success"){
            $responseArr = array(
                "code" => 200,
                "employees" => $DataArr["response"],
                "totalPages" => $DataArr["totalPages"]
            );
        }else{
            $responseArr = array(
                "code" => 500,
                "employees" => [],
                "totalPages" => 0
            );
        }
        return response()->json($responseArr);
    }
    public function getEmployeesById(){
        $data = json_decode(file_get_contents('php://input'), true);
        $DataArr = $this->Employee->getEmployeeById($data);
        if($DataArr["status"] == "success"){
            $responseArr = array(
                "code" => 200,
                "employees" => $DataArr["response"]
            );
        }else{
            $responseArr = array(
                "code" => 500,
                "employees" => []
            );
        }
        return response()->json($responseArr);
    }
    public function getEmployeeProjects(){
        $data = json_decode(file_get_contents('php://input'), true);
        $DataArr = $this->Employee->getEmployeeProjects($data);
        if($DataArr["status"] == "success"){
            $responseArr = array(
                "code" => 200,
                "projects" => $DataArr["response"]
            );
        }else{
            $responseArr = array(
                "code" => 500,
                "projects" => []
            );
        }
        return response()->json($responseArr);
    }
}