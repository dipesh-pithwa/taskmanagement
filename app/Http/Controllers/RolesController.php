<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use App\Models\Roles;
use Excel;
use Illuminate\Http\Request;

class RolesController extends Controller {
    public function __construct(Roles $roles) {
        $this->Roles = $roles;
    }

    public function getRoles(){
        $data = json_decode(file_get_contents('php://input'), true);
        $DataArr = $this->Roles->getRoles($data);
        if($DataArr["status"] == "success"){
            $responseArr = array(
                "code" => 200,
                "roles" => $DataArr["response"]
            );
        }else{
            $responseArr = array(
                "code" => 500,
                "roles" => ""
            );
        }
        return response()->json($responseArr);
    }
}