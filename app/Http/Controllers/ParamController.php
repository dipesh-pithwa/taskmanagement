<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use App\Models\Param;

class ParamController extends Controller {
    public function __construct(Param $param) {
        $this->Param = $param;
    }

    public function getAllParamList(){
        $data = json_decode(file_get_contents('php://input'), true);
        
        $ParamData = $this->Param->getAllParamList($data);

        if($ParamData["status"] == "success" && $ParamData['response']!=''){

            // print_r($ParamData['response']);die;

            
            // Send Email Here.
            $responseArr = array(
                "code"      => 200,
                "result"    => $ParamData["status"],
                "params"    => $ParamData['response']
            );
        }else{
            $responseArr = array(
                "code"      => 200,
                "result"    => $ParamData["status"],
                "params"    => "Invalid tenant, please contact system admin" 
            );
        }
        return response()->json($responseArr);
    }

    public function getParamList(){
        $data = json_decode(file_get_contents('php://input'), true);
        
        $ParamData = $this->Param->getParamList($data);

        if($ParamData["status"] == "success" && $ParamData['response']!=''){

            // print_r($ParamData['response']);die;

            
            // Send Email Here.
            $responseArr = array(
                "code"      => 200,
                "result"    => $ParamData["status"],
                "params"    => $ParamData['response'],
                "allParams" => $ParamData['allParams']
            );
        }else{
            $responseArr = array(
                "code"      => 200,
                "result"    => $ParamData["status"],
                "params"    => "Invalid tenant, please contact system admin",
                "allParams" => []
            );
        }
        return response()->json($responseArr);
    }
    public function getSelectedParamDetails(){
        $data = json_decode(file_get_contents('php://input'), true);
        $ParamData = $this->Param->getSelectedParamDetails($data);
        
        if($ParamData["status"] == "success" && count($ParamData['response'])>=1){
            $responseArr = array(
                "code"      => 200,
                "result"    => $ParamData["status"],
                "params"    => $ParamData['response']
            );
        }else{
            $responseArr = array(
                "code"      => 200,
                "result"    => $ParamData["status"],
                "params"    => "Invalid tenant, please contact system admin"
            );
        }
        return response()->json($responseArr);
    }
    public function getParamDetails(){
        $data = json_decode(file_get_contents('php://input'), true);
        $ParamData = $this->Param->getParamDetails($data);
        
        if($ParamData["status"] == "success" && count($ParamData['response'])>1){
            $responseArr = array(
                "code"      => 200,
                "result"    => $ParamData["status"],
                "params"    => $ParamData['response']
            );
        }else{
            $responseArr = array(
                "code"      => 200,
                "result"    => $ParamData["status"],
                "params"    => []
            );
        }
        return response()->json($responseArr);
    }
    public function saveParameter(){
        $data = json_decode(file_get_contents('php://input'), true);
        $ParamData = $this->Param->saveParameter($data);
        if($ParamData["status"] == "success" && $ParamData['response']!=''){
            // Send Email Here.
            $responseArr = array(
                "code"      => 200,
                "result"    => $ParamData["status"],
                "paramId"    => $ParamData['response']
            );
        }else{
            $responseArr = array(
                "code"      => 200,
                "result"    => $ParamData["status"],
                "paramId"    => "Failed to save record"
            );
        }
        return response()->json($responseArr);
    }
}