<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use App\Models\Login;

class LoginController extends Controller {
    public function __construct(Login $login) {
        $this->Login = $login;
    }

    public function login(){
        $data = json_decode(file_get_contents('php://input'), true);
        // print_r($data);die;
        $LoginData = $this->Login->login($data);

        // print_r($LoginData);die;
        if($LoginData["status"] == "success" && $LoginData['response']!=''){
            // Send Email Here.
            $responseArr = array(
                "code"          => 200,
                "tenantId"      => $LoginData['response']['TenantDetails']->Id,
                "companyName"   => $LoginData['response']['TenantDetails']->CompanyName,
                "firstName"     => $LoginData['response']['TenantDetails']->TenantName,
                "lastName"      => "",
                "role"          => $LoginData['response']['roleDetails']->ParamValue,
                "pageAccess"    => $LoginData['response']['pageAccess'],
                "message"       => "Login Successful",
                "userName"      => $LoginData['response']["UserName"],
                "employeeId"    => $LoginData['response']["Login"]->EmployeeId

            );
        }else{
            $responseArr = array(
                "code" => 200,
                "message" => "Invalid Username / Password"
            );
        }
        return response()->json($responseArr);
    }

    public function changePassword(){
        $data = json_decode(file_get_contents('php://input'), true);
        if($data["newPassword"] != $data["reenterPassword"]){
            $responseArr = array(
                "code" => 200,
                "message" => "new Password and reenter Password does not match."
            );
            return response()->json($responseArr);
        }
        $PasswordChangeData = $this->Login->changePassword($data);
        if($PasswordChangeData["status"] == "success" && $PasswordChangeData['response']!=''){
            // Send Email Here.
            $responseArr = array(
                "code" => 200,
                "result" => "success",
                "message" => "Your password has changed successfully. Please login again."
            );
        }else{
            $responseArr = array(
                "code" => 200,
                "message" => "Invalid Username / Old Password"
            );
        }
        return response()->json($responseArr);
    }

    public function forgotPassword(){
        $data = json_decode(file_get_contents('php://input'), true);
        $ForgotPasswordData = $this->Login->forgotPassword($data);
        if($ForgotPasswordData["status"] == "success"){
            $responseArr = array(
                "code" => 200,
                "result" => "success",
                "message" => "An email is sent to your registered email address with reset password link. Please check your mail box"
            );
        }else{
            $responseArr = array(
                "code" => 200,
                "message" => "Unable to find User"
            );
        }
        return response()->json($responseArr);
    }
}