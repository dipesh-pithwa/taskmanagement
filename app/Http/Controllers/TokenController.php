<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use App\Models\ApiToken;
use Excel;
use Illuminate\Http\Request;

class TokenController extends Controller {
    public function __construct(ApiToken $apiToken) {
        $this->ApiToken = $apiToken;
    }

    public function generateToken(){
        $data = json_decode(file_get_contents('php://input'), true);
        $DataArr = $this->ApiToken->generateToken($data);
        if($DataArr["status"] == "success"){
            $responseArr = array(
                "code" => 200,
                "response" => $DataArr["response"]
            );
        }else{
            $responseArr = array(
                "code" => 500,
                "response" => "Unable to Update."
            );
        }
        return response()->json($responseArr);
    }
}