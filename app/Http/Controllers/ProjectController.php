<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use App\Models\Project;

class ProjectController extends Controller {
    public function __construct(Project $project) {
        $this->Project = $project;
    }

    public function getAllCRM(){
        $data = json_decode(file_get_contents('php://input'), true);
        $CRMResult = $this->Project->getAllCRMProjects($data);
        if($CRMResult["status"] == "success"){
            $responseArr = array(
                "code" => 200,
                "data" => $CRMResult["response"],
                "projects" => $CRMResult["projects"]
            );
        }else{
            $responseArr = array(
                "code" => 500,
                "message" => "Error Occured"
            );
        }
        return response()->json($responseArr);
    }

    public function saveCRM(){
        $data = json_decode(file_get_contents('php://input'), true);
        $CRMResult = $this->Project->saveCRM($data);
        if($CRMResult["status"] == "success"){
            $responseArr = array(
                "code" => 200,
                "data" => $CRMResult["response"]
            );
        }else{
            $responseArr = array(
                "code" => 500,
                "message" => "Error Occured"
            );
        }
        return response()->json($responseArr);
    }

    public function saveSiteDetails(){
        $data = json_decode(file_get_contents('php://input'), true);
        $ProjectData = $this->Project->saveSiteDetails($data);
        if($ProjectData["status"] == "success"){
            $responseArr = array(
                "code" => 200,
                "message" => "Project site details updated successfully"
            );
        }else{
            $responseArr = array(
                "code" => 500,
                "message" => "Error Occured"
            );
        }
        return response()->json($responseArr);
    }

    public function searchProject(){
        $data = json_decode(file_get_contents('php://input'), true);
        $ProjectData = $this->Project->searchProject($data);

       
        if($ProjectData["status"] == "success" && count($ProjectData["response"]) > 0){

            $ProjectArr = array();
            foreach ($ProjectData["response"] as $key => $value) {
                array_push(
                    $ProjectArr,
                    array(
                        "projectId" => $value["Id"],
                        "projectName" => $value["ProjectName"],
                        "projectCode" => $value["ProjectCode"]
                    )
                );
            }
            $responseArr = array(
                "code" => 200,
                "projects" => $ProjectArr,
                "totalPages" => $ProjectData["totalPages"]
            );
        }elseif($ProjectData["status"] == "success" && count($ProjectData["response"]) == 0){
            $responseArr = array(
                "code" => 200,
                "projects" => [],
                "totalPages" => 1
            );
        }else{
            $responseArr = array(
                "code" => 500,
                "message" => "Error Occured"
            );
        }
        return response()->json($responseArr);
    }

    public function getProjectAccess(){
        $data = json_decode(file_get_contents('php://input'), true);
        $ProjectData = $this->Project->getProject($data);

        if($ProjectData["status"] == "success" && count($ProjectData["response"]) > 0){
            // print_r($ProjectData);die;
            // Send Email Here.
            // Employee Name, Project Name and Role Name
            $data["ProjectName"] = $ProjectData['response'][0]->ProjectName;
            $this->Project->sendEmailProjectAccess($data);
            $responseArr = array(
                "code" => 200,
                "message" => "Your request to get access on project '".$ProjectData['response'][0]->ProjectName."' is sent to system admin"
            );
        }elseif($ProjectData["status"] == "success" && count($ProjectData["response"]) == 0){
            $responseArr = array(
                "code" => 200,
                "message" => "Project does not exists"
            );
        }else{
            $responseArr = array(
                "code" => 500,
                "message" => "Error Occured"
            );
        }
        return response()->json($responseArr);
    }

    public function saveProjectDetails(){

        $data = json_decode(file_get_contents('php://input'), true);
        // print_r($data);die;
        $ProjectData = $this->Project->saveProjectDetails($data);

        if($ProjectData["status"] == "success" && $ProjectData != ""){
            // Send Email Here.
            $responseArr = array(
                "code" => 200,
                "projectId" => $ProjectData["response"],
                "message" => "Project details updated successfully"
            );
        }else{
            $responseArr = array(
                "code" => 500,
                "projectId" => "",
                "message" => "Error Occured"
            );
        }
        return response()->json($responseArr);

    }

    public function getProjectDetails(){

        $data = json_decode(file_get_contents('php://input'), true);
        // print_r($data);die;
        $ProjectData = $this->Project->getProjectDetails($data);

        // print_r($ProjectData);die;

        if($ProjectData["status"] == "success" && count($ProjectData["response"]) > 0){
            // Send Email Here.
            $responseArr = $ProjectData['response'];
        }elseif($ProjectData["status"] == "success" && count($ProjectData["response"])==0){
            $responseArr = array(
                "code" => 200,
                "message" => "Invalid project id"
            );
        }else{
            $responseArr = array(
                "code" => 500,
                "message" => "Error Occured"
            );
        }
        return response()->json($responseArr);

    }
}