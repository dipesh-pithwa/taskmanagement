<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Redirect;
use Session,Lib,Config,DB,Lang,Request,Response,Mail;
class UIControllerOne extends Controller{
	
	public function __construct()
	{

	}

	public function ProvideAccess($UserId, $ProjectId){
		$RoleId = DB::table("SystemParams")->where("ParamName", "Role")->where("ParamValue", "Project Manager")->first();
		$RoleIdJSON = json_encode($RoleId);
		$RoleId = json_decode($RoleIdJSON, true);
		// $RoleId["Id"]

		$UserData = DB::table("UserDetails")->where("Id", $UserId)->first();
		$UserDataJSON = json_encode($UserData);
		$UserData = json_decode($UserDataJSON, true);

		$EmployeeData = DB::table("EmployeeDetails")->where("EmailAddress", $UserData["EmailAddress"])->first();
		$EmployeeDataJSON = json_encode($EmployeeData);
		$EmployeeData = json_decode($EmployeeDataJSON, true);
		$EmployeeId = $EmployeeData["Id"];

		$ProjectTeamDetail = DB::table("ProjectTeamDetails")->where("EmployeeId", $EmployeeId)->where("ProjectId", $ProjectId)->get();
		if(count($ProjectTeamDetail) > 0){
			// Update
			DB::table("ProjectTeamDetails")->where("EmployeeId", $EmployeeId)->where("ProjectId", $ProjectId)->update(["RoleId" => $RoleId["Id"]]);
		}else{
			// Insert
			DB::table("ProjectTeamDetails")->insert(["ProjectId" => $ProjectId, "EmployeeId" => $EmployeeId, "RoleId" => $RoleId["Id"]]);
		}
		echo "Access Granted.";
	}

	public function sendEmails(){
		$EmailData = DB::table("EmailQueue")->where("Status", "")->get();
		$EmailDataJSON = json_encode($EmailData);
		$EmailData = json_decode($EmailDataJSON, true);
		foreach ($EmailData as $key => $value) {
			Mail::send('emailer.forgot', $value, function ($message) use($value) {
				$EmailArr = explode(",", $value["EmailTo"]);
				foreach ($EmailArr as $key2 => $value2) {
					$message->to($value2, $value2);
				}
            	$message->subject($value["EmailSubject"]);
        	});
        	DB::table("EmailQueue")->where("Id", $value["Id"])->update(["Status" => "sent"]);
		}
	}

	public function ForgotPassword($EmailId, $Password){
		$postdata = Request::all();
		$viewArr = array(
			"EmailId" => base64_decode($EmailId),
			"Password" => base64_decode($Password)
		);
		return view("forgot_password", $viewArr);
	}

	public function logout(){
		Session::flush();
		return redirect('/login');
	}

	public function login()
	{
		return view('login');
	}

	public function doLogin(){
		$postdata = Request::all();

		$requestData = array(
			'username'=>$postdata['txtEmailId'],
			'password'=>$postdata['txtPassword'],
		);

		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://localhost/livepages/index.php/api/login",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => json_encode($requestData),
		  CURLOPT_HTTPHEADER => array(
		    "Content-Type: application/json",
		    "cache-control: no-cache"
		  ),
		));

		$response = curl_exec($curl);
		curl_close($curl);

		$res = json_decode($response);

		$retData['status']=0; 
		$retData['valid'] =0; 

		if($res->code==200){
			if($res->message=='Login Successful'){
				$retData['status']=1; 
				$retData['valid'] =1; 
			}			
		}
		
		echo json_encode($retData);
	}
}