<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\UserNotification;
use DB;

class Project extends Model {
	public function __construct(UserNotification $usernotification) {
        $this->UserNotification = $usernotification;
    }

    public function sendEmailProjectAccess($data){
        try {
            $Name = DB::table("UserDetails as u")
                ->join("EmployeeDetails as e", "u.EmailAddress", "=", "e.EmailAddress")
                ->select("e.EmployeeName", "u.TenantId")
                ->where("u.Id", $data["userId"])
                ->first();

            $NameJSON = json_encode($Name);
            $Name = json_decode($NameJSON, true);

            $SystemAdminEmails = DB::table("UserDetails as u")
                ->join("SystemParams as s", "s.Id", "=", "u.RoleId")
                ->where("s.ParamValue", "System Admin")
                ->select("u.EmailAddress")
                ->get();

            $SystemAdminEmailsJSON = json_encode($SystemAdminEmails);
            $SystemAdminEmails = json_decode($SystemAdminEmailsJSON, true);

            $EmailArr = [];
            foreach ($SystemAdminEmails as $key => $value) {
                array_push($EmailArr, $value["EmailAddress"]);
            }
            $EmailIds = implode(",", $EmailArr);

            DB::table("EmailQueue")
                ->insert(array(
                    "EmailTo" => $EmailIds,
                    "EmailSubject" => "Project Access",
                    "EmailBody" => "Employee: ".$Name["EmployeeName"]." needs access to project '".$data["ProjectName"]."' as ".$data["role"]." <a href='http://localhost/livepages/provideAccess/".$data["userId"]."/".$data["projectId"]."'>Click here</a> to give access.",
                    "Status" => "",
                    "DateSent" => date("Y-m-d H:i:s"),
                    "EmailType" => "Get Project Access"
                ));
        } catch (Exception $e) {
            
        }
    }

    public function getAllCRMProjects(){
        try {
            $OpportunityResult = DB::select("select a.id, a.name, a.date_entered, a.sales_stage, c.id as contactId, c.first_name, c.last_name, c.phone_mobile, c.phone_work from opportunities a inner join opportunities_contacts b on a.id = b.opportunity_id inner join contacts c on b.contact_id = c.id");
            $OpportunityResultJSON = json_encode($OpportunityResult);
            $OpportunityResult = json_decode($OpportunityResultJSON, true);

            $ProjectNames = array();
            foreach ($OpportunityResult as $key => $value) {
                if(!in_array($value["name"], $ProjectNames)){
                    array_push($ProjectNames, $value["name"]);
                }
            }

            return array(
                "status" => "success",
                "code" => 200,
                "response" => $OpportunityResult,
                "projects" => $ProjectNames
            );
        } catch (Exception $e) {
            return array("status" => "failed", "code" => 500, "response" => "");
        }
    }

    public function saveCRM($data){
        // select a.id, a.name, a.date_entered, a.sales_stage, c.id as contactId, c.first_name, c.last_name, c.phone_mobile, c.phone_work from opportunities a inner join opportunities_contacts b on a.id = b.opportunity_id inner join contacts c on b.contact_id = c.id
        try {
            $OpportunityResult = DB::select("select a.id, a.name, a.date_entered, a.sales_stage, c.id as contactId, c.first_name, c.last_name, c.phone_mobile, c.phone_work from opportunities a inner join opportunities_contacts b on a.id = b.opportunity_id inner join contacts c on b.contact_id = c.id where a.name = ? and a.date_entered = ?", [$data["projectName"], $data["enquiryDate"]]);
            $OpportunityResultJSON = json_encode($OpportunityResult);
            $OpportunityResult = json_decode($OpportunityResultJSON, true);

            $Project1 = DB::table("ProjectDetails")->where("ProjectName", $data["projectName"])->get();

            if(count($OpportunityResult) > 0 && count($Project1) < 1){
                // Code to generate ProjectCode.
                $TenantData = DB::table("TenantDetails")->where("Id", $data['tenantId'])->first();
                $TenantDataJSON = json_encode($TenantData);
                $TenantData = json_decode($TenantDataJSON, true);

                $CompanyNameWordArr = explode(" ", $TenantData["CompanyName"]);
                $Initials = substr(@$CompanyNameWordArr[0], 0, 1) . substr(@$CompanyNameWordArr[1], 0, 1);

                $ProjectInsData = array(
                    "TenantId" => $data["tenantId"],
                    "ProjectName" => $OpportunityResult[0]["name"],
                    "OpportunityId" => $OpportunityResult[0]["id"],
                    "EnquiryDate" => $OpportunityResult[0]["date_entered"],
                );

                $Project = DB::table("ProjectDetails")->insertGetId($ProjectInsData);
                $ProjectCode = $Initials.str_pad($Project,4,"0",STR_PAD_LEFT);
                DB::table("ProjectDetails")->where("Id", $Project)->update(["ProjectCode" => $ProjectCode]);

                foreach ($OpportunityResult as $key => $value) {
                    $insDataContact = array(
                        "ProjectId" => $Project,
                        "ContactName" => $value["first_name"]." ".$value["last_name"],
                        "MobileNumber" => $value["phone_mobile"] == ""?$value["phone_work"]:$value["phone_mobile"]
                    );
                    DB::table("ProjectContactDetails")->insert($insDataContact);
                }
                return array(
                    "status" => "success",
                    "code" => 200,
                    "response" => "Project Saved"
                );
            }else{
                return array(
                    "status" => "success",
                    "code" => 200,
                    "response" => "Project Already Exists"
                );
            }
        } catch (Exception $e) {
            return array("status" => "failed", "code" => 500, "response" => "");
        }
    }

    public function saveSiteDetails($data){
        try {
            $DataArr = array(
                "ProjectId"     => $data["projectId"],
                "SiteName"      => $data["site"]["siteName"],
                "AddressLine1"  => $data["site"]["addressLine1"],
                "AddressLine2"  => $data["site"]["addressLine2"],
                "City"          => $data["site"]["city"],
                "State"         => $data["site"]["state"],
                "ZipCode"       => $data["site"]["zipCode"],
                "Country"       => $data["site"]["country"]
            );
            if($data["site"]["id"] != ""){
                // Update
                DB::table("ProjectSiteDetails")->where("Id", $data["site"]["id"])->update($DataArr);
                return array("status" => "success", "response" => "Data Updated");
            }else{
                // Insert
                DB::table("ProjectSiteDetails")->insert($DataArr);
                return array("status" => "success", "response" => "Data Created");
            }
        } catch (Exception $e) {
            return array("status" => "failed", "response" => "");
        }
    }

    public function searchProject($data){
        try {
            $Offset = ($data["pageNumber"] * $data["pageSize"]) - $data["pageSize"];
            if($data["projectName"] == ""){
                $Project = DB::table("ProjectDetails")->orderBy('CreatedOn', 'desc')->orderBy("ProjectName", "asc")->offset($Offset)->limit($data["pageSize"])->get();
                $ProjectDataCount = DB::table("ProjectDetails")->count();
            }else{
                $Project = DB::table("ProjectDetails")->where("ProjectName", 'like', '%'.$data["projectName"].'%')->orderBy("ProjectName", "asc")->offset($Offset)->limit($data["pageSize"])->get();
                $ProjectDataCount = DB::table("ProjectDetails")->where("ProjectName", 'like', '%'.$data["projectName"].'%')->count();
            }
            $ProjectJSON = json_encode($Project);
            $Project = json_decode($ProjectJSON, true);

            
            $TotalPageCount = ceil($ProjectDataCount / $data["pageSize"]);
            return array("status" => "success", "response" => $Project, "totalPages" => $TotalPageCount);
        } catch (Exception $e) {
            return array("status" => "failed", "response" => []);
        }
    }

    public function getProject($data){
    	try {
    		$Project = DB::table("ProjectDetails")->where("Id", $data["projectId"])->get();

            // print_r($Project[0]);die;
            if(count($Project) > 0){
                $NotifyData = array(
                "UserId" => $data["userId"],
                "NotificationTitle" => "Request for Project Access",
                "NotificationText" => "User have requested access for project ".$Project[0]->ProjectName,
                "RelativeLink" => "http://link.com"
            );
            $this->UserNotification->Notify($NotifyData);
            }
    		return array("status" => "success", "response" => $Project);
        } catch (Exception $e) {
            return array("status" => "failed", "response" => []);
        }	
    } 

    public function saveProjectDetails($data){
        try {

            $projectDataUpdate = array(
                'ProjectName'          => $data['projectName'],
                'TenantId'             => $data['tenantId'],
                // ''                  => $data['crmId'],
                'EnquiryDate'          => $data['enquiryDate'],
                'ReferredBy'           => $data['referredBy'],
                'ProjectStatus'        => $data['projectStatus'],
                // ''                  => $data['projectState'],
                'ProjectDiscipline'    => $data['projectDiscipline'],
                'ProjectType'          => $data['projectType'],
                'ProjectSubType'       => $data['projectSubType'],
                'ProjectCategory'      => $data['projectCategory'],
                'EnquiryValue'         => $data['enquiryValue'],
                'EnquiryDesignFee'     => $data['enquiryDesignFee'],
                'AppointmentDate'      => $data['appointmentDate'] == ""? NULL:$data['appointmentDate'],
                'LostDate'             => $data['lostDate'] == ""? NULL:$data['lostDate'],
                'ProjectStartDate'     => $data['projectStartDate'] == ""? NULL:$data['projectStartDate'],
                'ProjectHoldDate'      => $data['projectHoldDate'] == ""? NULL:$data['projectHoldDate'],
                'CompletionTargetDays' => $data['completionTarget'],
                'TargetDate'           => $data['targetDate'] == ""? NULL:$data['targetDate'],
                'ActualCompletionDate' => $data['actualCompletionDate'] == ""? NULL:$data['actualCompletionDate'],
                'JobStatus'            => $data['jobStatus'],
                'AwardedProjectValue'  => $data['awardedProjectValue'],
                // ''                  => $data['awardedDesignValue'],
                'Area'                 => $data['area'],
                'CreatedBy'            => $data['createdBy'],
                'CreatedOn'            => date("Y-m-d H:i:s"),
                'ModifiedBy'           => $data['modifiedBy'],



            );

            if($data["projectId"] == ""){
                $TenantData = DB::table("TenantDetails")->where("Id", $data['tenantId'])->first();
                $TenantDataJSON = json_encode($TenantData);
                $TenantData = json_decode($TenantDataJSON, true);

                $CompanyNameWordArr = explode(" ", $TenantData["CompanyName"]);
                $Initials = substr(@$CompanyNameWordArr[0], 0, 1) . substr(@$CompanyNameWordArr[1], 0, 1);

                $Project = DB::table("ProjectDetails")->insertGetId($projectDataUpdate);

                $ProjectCode = $Initials.str_pad($Project,4,"0",STR_PAD_LEFT);
                DB::table("ProjectDetails")->where("Id", $Project)->update(["ProjectCode" => $ProjectCode]);
            }else{
                $Project = DB::table("ProjectDetails")->where("Id", $data["projectId"])->update($projectDataUpdate);
            }
            $ProjectJSON = json_encode($Project);
            $Project = json_decode($ProjectJSON, true);
            return array("status" => "success", "response" => $Project);
        } catch (Exception $e) {
            return array("status" => "failed", "response" => []);
        }   
    }

    public function getProjectDetails($data){

        try {

            $projectDetails='';
            
            $ProjectTemp = DB::table("ProjectDetails")->where("Id", $data["projectId"])->first();

            // print_r($ProjectTemp);die;

            if(!empty($ProjectTemp)){

                $ContactDetails     = DB::table('ProjectContactDetails')->where('ProjectId',$data["projectId"])->first();
                $ProjectSiteDetails = DB::table('ProjectSiteDetails')->where('ProjectId',$data["projectId"])->first();

                $Project    = (array)$ProjectTemp;

                $TeamDetails = DB::table("ProjectTeamDetails as p")
                ->join("EmployeeDetails as e", "p.EmployeeId", "=", "e.Id")
                ->join("SystemParams as s", "p.RoleId", "=", "s.Id")
                ->select("p.Id as teamId", "p.EmployeeId as employeeId", "e.EmployeeName as employeeName", "p.RoleId as roleId", "p.StartDate as startDate", "p.EndDate as endDate", "p.AllocatedHours as allocatedHours", "p.DateCreated as addedOn", "p.CreatedBy as addedBy", "p.DateModified as modifiedOn", "p.Modifiedby as modifiedBy", "s.ParamValue as role")
                ->where('ProjectId',$data["projectId"])
                ->get();
                $TeamDetailsJSON = json_encode($TeamDetails);
                $TeamDetails = json_decode($TeamDetailsJSON, true);

                $projectDetails = array(
                    'code'                   => 200,
                    'projectName'            => $Project['ProjectName'],
                    'projectSource'          => '',//$Project['ProjectSource'],
                    'crmId'                  => '',//$Project['CrmId'],
                    'enquiryDate'            => $Project['EnquiryDate'],
                    'referredBy'             => $Project['ReferredBy'],
                    'projectStatus'          => $Project['ProjectStatus'],
                    'projectState'           => '',//$Project['ProjectState'],
                    'projectDiscipline'      => $Project['ProjectDiscipline'],
                    'projectType'            => $Project['ProjectType'],
                    'projectSubType'         => $Project['ProjectSubType'],
                    'projectCategory'        => $Project['ProjectCategory'],
                    'enquiryValue'           => $Project['EnquiryValue'],
                    'enquiryDesignFee'       => $Project['EnquiryDesignFee'],
                    'appointmentDate'        => $Project['AppointmentDate'],
                    'lostDate'               => $Project['LostDate'],
                    'projectStartDate'       => $Project['ProjectStartDate'],
                    'projectHoldDate'        => $Project['ProjectHoldDate'],
                    'completionTarget'       => $Project['CompletionTargetDays'],
                    'targetDate'             => $Project['TargetDate'],
                    'actualCompletionDate'   => $Project['ActualCompletionDate'],
                    'jobStatus'              => $Project['JobStatus'],
                    'awardedProjectValue'    => $Project['AwardedProjectValue'],
                    'awardedDesignValue'     => '',//$Project['AwardedDesignValue'],
                    'area'                   => $Project['Area'],
                    'projectCode'            => $Project['ProjectCode'],
                    'contacts'               => array(
                                                    'name'  => @$ContactDetails->ContactName,
                                                    'phone' => @$ContactDetails->MobileNumber,
                                                    'email' => @$ContactDetails->EmailAddress,
                                                ),   
                    'site'                   => array(
                                                    'addressLine1' =>@$ProjectSiteDetails->AddressLine1,
                                                    'addressLine2' =>@$ProjectSiteDetails->AddressLine2,
                                                    'city'         =>@$ProjectSiteDetails->City,
                                                    'state'        =>@$ProjectSiteDetails->State,
                                                    'country'      =>@$ProjectSiteDetails->Country,
                                                    'zipCode'      =>@$ProjectSiteDetails->ZipCode,
                                                ),
                    'team'                   => $TeamDetails,
                    'area'                   => $Project['Area'],
                    'createdBy'              => $Project['CreatedBy'],
                    'dateCreated'            => $Project['CreatedOn'],
                    'modifiedBy'             => $Project['ModifiedBy'],
                    'ctc'                    => array(

                                                    'attribute1'=>'',
                                                    'attribute2'=>'',
                                                    'attribute3'=>'',
                                                    'attribute4'=>'',
                                                ),
                    'lastModified'           =>''



                );
            }

            return array("status" => "success", "response" => $projectDetails);
        } catch (Exception $e) {
            return array("status" => "failed", "response" => []);
        }   
    }
}