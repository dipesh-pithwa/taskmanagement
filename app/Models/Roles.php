<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Roles extends Model {
	public function __construct() {
    }
    public function getRoles($data){
    	try {
    		$Roles = DB::table("SystemParams")->where("TenantId", $data["tenantId"])->where("ParamName", "Role")->get();

            $RolesJSON = json_encode($Roles);
            $Roles = json_decode($RolesJSON, true);

            $Response = array();
            foreach ($Roles as $key => $value) {
                array_push(
                    $Response, array(
                        "id" => $value["Id"], // 1,
                        "tenantId" => $value["TenantId"],
                        "paramName" => $value["ParamName"],
                        "paramValue" => $value["ParamValue"],// "System Admin",
                        "parentId" => $value["ParentId"],// 0,
                        "status" => $value["Status"], // "Active",
                        "dateCreated" => $value["DateCreated"], // "2019-03-15 10:54:41",
                        "dateModified" => $value["DateModified"], // null
                    )
                );
            }
            

    		return array("status" => "success", "response" => $Response);
        } catch (Exception $e) {
            return array("status" => "failed", "response" => []);
        }	
    }
}