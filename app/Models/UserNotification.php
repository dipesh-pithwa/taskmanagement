<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class UserNotification extends Model {
    public function Notify($data){
    	try {
    		$data["ReadStatus"] = 0;
    		$Project = DB::table("UserNotification")->insert($data);
    		return array("status" => "success", "response" => $Project);
        } catch (Exception $e) {
            return array("status" => "failed", "response" => []);
        }	
    }

    public function getUserNotifications($data){
    	try {
    		$NotificationData = DB::table("UserNotification")
    			->where("UserId", $data["userId"])
    			->get();
    		return array("status" => "success", "response" => $NotificationData);
    	} catch (Exception $e) {
    		return array("status" => "failed", "response" => []);
    	}
    }

    public function updateNotification($data){
    	try {
    		DB::table("UserNotification")
            ->where("Id", $data["notificationId"])
            ->update(array("ReadStatus" => "Read", "DateRead" => date("Y-m-d H:i:s")));
            return array("status" => "success", "response" => "Notification marked as read");
    	} catch (Exception $e) {
    		return array("status" => "failed", "response" => []);
    	}
    }
}
