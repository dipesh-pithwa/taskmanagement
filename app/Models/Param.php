<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Param extends Model {
	public function __construct(UserNotification $usernotification) {
    }
    public function getAllParamList($data){
        try {
            $ParamDetails = DB::table("SystemParams")->get();
            $ParamDetailsJSON = json_encode($ParamDetails);
            $ParamDetails = json_decode($ParamDetailsJSON, true);

            $finalResponse = array();
            foreach ($ParamDetails as $key => $value) {
                array_push(
                    $finalResponse,
                    array(
                        "paramName" => $value["ParamName"],
                        "paramValue" => $value["ParamValue"],
                        "status" => $value["Status"],
                        "description" => $value["Description"],
                        "orderCount" => $value["OrderCount"]
                    )
                );
            }
            return array("status" => "success", "response" => $finalResponse);
        } catch (Exception $e) {
            return array("status" => "failed", "response" => []);
        }   
    }
    public function getParamList($data){
        try {
            $TenantDetails = DB::table("TenantDetails")->where("Id",$data["tenantId"])->first();
            if(!empty($TenantDetails)){
                // a = select * from SystemParams where TenantId = 1 and ParamName = CompanyName
                // b = select * from SystemParams where ParentId = a.Id and ParamName = Department
                // c = select * from SystemParams where ParentId = b.Id

                $AllParams = DB::table("SystemParams")->select("Id as paramId", "ParamValue as paramName")->where("TenantId", $data["tenantId"])->orderBy("ParamValue", "asc")->get();
                $AllParamsJSON = json_encode($AllParams);
                $AllParams = json_decode($AllParamsJSON, true);

                $CompanyData = DB::table("SystemParams")->where("TenantId", $data["tenantId"])->where("ParamName", "Company Name")->get();
                $CompanyDataJSON = json_encode($CompanyData);
                $CompanyData = json_decode($CompanyDataJSON, true);

                $finalResponse = [];
                $Counter = 0;
                foreach ($CompanyData as $key => $value) {
                    $TenantDetails = DB::table("TenantDetails")->where("CompanyName", $value["ParamValue"])->first();
                    $TenantDetailsJSON = json_encode($TenantDetails);
                    $TenantDetails = json_decode($TenantDetailsJSON, true);
                    $finalResponse["params"][$Counter] = array("companyId" => $TenantDetails["Id"], "companyName" => $value["ParamValue"]);

                    $DepartmentData = DB::table("SystemParams")->where("ParentId", $value["Id"])->where("ParamName", "Department")->orderBy("ParamValue", "asc")->get();
                    $DepartmentDataJSON = json_encode($DepartmentData);
                    $DepartmentData = json_decode($DepartmentDataJSON, true);
                    $Counter2 = 0;
                    foreach ($DepartmentData as $key2 => $value2) {
                        // $finalResponse["params"][$Counter]["departments"][$Counter2] = array("departmentName" => $value2["ParamValue"]);
                        $ParamData = DB::table("SystemParams")->where("ParentId", $value2["Id"])->orderBy("ParamName", "asc")->get();
                        $ParamDataJSON = json_encode($ParamData);
                        $ParamData = json_decode($ParamDataJSON, true);

                        $ParamArr = array();
                        foreach ($ParamData as $key3 => $value3) {
                            $ParamArr[] = array("paramId" => $value3["Id"], "paramName" => $value3["ParamName"]);
                        }
                        $finalResponse["params"][$Counter]["departments"][$Counter2] = array("departmentName" => $value2["ParamValue"], "params" => $ParamArr);
                        $Counter2++;
                    }
                    $Counter++;
                }
            }else{
                $finalResponse='';
            }
            return array("status" => "success", "response" => $finalResponse["params"], "allParams" => $AllParams);
        } catch (Exception $e) {
            return array("status" => "failed", "response" => [], "allParams" => []);
        }
        // try {
        //         // $ParamDetails = DB::table("EmployeeDetails")->select('CompanyName')->where("TenantId", $data["tenantId"])->get();
        //         $TenantDetails = DB::table("TenantDetails")->where("Id",$data["tenantId"])->first();

        //         if(!empty($TenantDetails)){

        //             $DepartmentArray = array();
        //             $ProjectParam  = array();
        //             $reposneArray['companyId']  =$TenantDetails->Id;
        //             $reposneArray['companyName']=$TenantDetails->CompanyName;
        //             $reposneArray['departments']= array();

        //             $DepartmentDetails = DB::table("EmployeeDetails")->select('Department','Id')->where("TenantId", $TenantDetails->Id)->get();

        //             // print_r($DepartmentDetails);die;

        //             foreach ($DepartmentDetails as $key => $value) {
        //                 $DepartmentArray['deprtmentName'] = $value->Department;



        //                 $ProjectTeamDetails = DB::table("ProjectTeamDetails")->select('ProjectId')->where("EmployeeId", $value->Id)->first();
        //                 // print_r($ProjectTeamDetails);die;

        //                 // print_r($ProjectTeamDetails);die;
        //                 $projectDetails = DB::table("ProjectDetails")->where("Id", $ProjectTeamDetails->ProjectId)->first();

        //                 $DepartmentArray['params'] = array(
        //                         "projectStatus"        => $projectDetails->ProjectStatus,
        //                         "projectDiscipline"    => $projectDetails->ProjectDiscipline,
        //                         "projectCategory"      => $projectDetails->ProjectCategory,
        //                         "jobStatus"            => $projectDetails->JobStatus,
        //                 );

        //                 array_push($reposneArray['departments'],$DepartmentArray);
        //             }

        //             $finalResponse = $reposneArray;
                    
        //         }else{
        //             $finalResponse='';
        //         }



        //     return array("status" => "success", "response" => $finalResponse);
        // } catch (Exception $e) {
        //     return array("status" => "failed", "response" => []);
        // }   
    }

    public function getSelectedParamDetails($data){
        try {
                $ParamDetails = DB::table("SystemParams")->where("TenantId", $data["tenantId"])->where("ParamName", $data["paramName"])->get();
                $ParamDetailsJSON = json_encode($ParamDetails);
                $ParamDetails = json_decode($ParamDetailsJSON, true);
                $Response = array();
                foreach ($ParamDetails as $key => $value) {
                    array_push(
                        $Response,
                        array(
                            "id" => $value["Id"],
                            "tenantId" => $value["TenantId"],
                            "paramName" => $value["ParamName"],
                            "paramValue" => $value["ParamValue"],
                            "description" => $value["Description"],
                            "orderCount" => $value["OrderCount"],
                            "parentId" => $value["ParentId"],
                            "status" => $value["Status"],
                            "dateCreated" => $value["DateCreated"],
                            "dateModified" => $value["DateModified"]
                        )
                    );
                }
                return array("status" => "success", "response" => $Response);
        } catch (Exception $e) {
            return array("status" => "failed", "response" => []);
        }   
    } 

    public function getParamDetails($data){
        try {

                $ParamDetailsParent = DB::table("SystemParams")->where("TenantId", $data["tenantId"])->where('ParamName',$data['paramName'])->get();

                $ParamDetailsParentJSON = json_encode($ParamDetailsParent);
                $ParamDetailsParent = json_decode($ParamDetailsParentJSON, true);

                $ParentIdArr = array();
                foreach ($ParamDetailsParent as $key2 => $value2) {
                    $ParentIdArr[] = $value2["Id"];
                }

                $ParamDetails = DB::table("SystemParams")->where("TenantId", $data["tenantId"])->whereIn('ParentId',$ParentIdArr)->get();
                $ParamDetailsJSON = json_encode($ParamDetails);
                $ParamDetails = json_decode($ParamDetailsJSON, true);

                $Response = array();
                foreach ($ParamDetails as $key => $value) {
                    array_push(
                        $Response,
                        array(
                            "id" => $value["Id"],
                            "tenantId" => $value["TenantId"],
                            "paramName" => $value["ParamName"],
                            "paramValue" => $value["ParamValue"],
                            "description" => $value["Description"],
                            "orderCount" => $value["OrderCount"],
                            "parentId" => $value["ParentId"],
                            "status" => $value["Status"],
                            "dateCreated" => $value["DateCreated"],
                            "dateModified" => $value["DateModified"]
                        )
                    );
                }
                

                return array("status" => "success", "response" => $Response);
        } catch (Exception $e) {
            return array("status" => "failed", "response" => []);
        }   
    }

    public function saveParameter($data){
        try {

                $inData = array(
                    
                    'TenantId'      => $data['tenantId'],
                    'ParamName'     => $data['paramName'],
                    'Paramvalue'    => $data['paramValue'],
                    'ParentId'      => $data['parentId'],
                    'Status'        => $data['status'],
                    'Description'   => $data['name'],
                    'OrderCount'    => $data['order']

                );

                if(@$data["paramId"] != ""){
                    DB::table("SystemParams")->where("Id", $data["paramId"])->update($inData);
                    $ParamDetails = $data["paramId"];
                }else{
                    $ParamDetails = DB::table("SystemParams")->insertGetId($inData);
                }

                return array("status" => "success", "response" => $ParamDetails);
        } catch (Exception $e) {
            return array("status" => "failed", "response" => []);
        }   
    } 
}