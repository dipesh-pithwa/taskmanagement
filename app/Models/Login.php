<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Login extends Model {
	public function __construct(UserNotification $usernotification) {
    }
    public function login($data){
    	try {
        		$Login = DB::table("UserDetails")->join("EmployeeDetails", "UserDetails.EmailAddress", "=", "EmployeeDetails.EmailAddress")->select("UserDetails.*", "EmployeeDetails.Id as EmployeeId")->where("UserDetails.EmailAddress", $data["username"])->where('UserDetails.Password',hash("SHA256", $data['password']))->where('UserDetails.Status','Active')->first();


                if(!empty($Login)){

                    $TenantDetails = DB::table("TenantDetails")->where("Id", $Login->TenantId)->first();
                    $PageAccessRightsDetails = DB::table("PageAccessRights")->where("RoleId", $Login->RoleId)->get();
                    $roleDetails = DB::table("SystemParams")->where("Id", $Login->RoleId)->first();

                    $responseArray['Login'] = $Login;
                    $responseArray['TenantDetails'] = $TenantDetails;
                    $responseArray['UserName'] = @$EmployeeData->EmployeeName;
                    // $responseArray['PageAccessRightsDetails'] = $PageAccessRightsDetails;
                    $responseArray['roleDetails'] = $roleDetails;   

                    // print_r($responseArray);die;

                    $finalArrayResponse=array();

                    foreach ($PageAccessRightsDetails as $key => $value) {

                        // print_r($value);die;
                        $PageAccessRightsDetailsTemp['pageName'] =$value->PageName;
                        $PageAccessRightsDetailsTemp['access']   =$value->Access;
                        
                        array_push($finalArrayResponse,$PageAccessRightsDetailsTemp);
                    
                    }

                    $responseArray['pageAccess'] = $finalArrayResponse;
                }else{
                    $responseArray='';
                }

                // print_r($finalArrayResponse);die;
		        return array("status" => "success", "response" => $responseArray);
        } catch (Exception $e) {
            return array("status" => "failed", "response" => []);
        }	
    }
    public function changePassword($data){
        try {
            if($data["encPass"] == "1"){
                $HashedPassword = $data['oldPassword'];
            }else{
                $HashedPassword = hash("SHA256", $data['oldPassword']);
            }
            
            $UserData = DB::table("UserDetails")->where("EmailAddress", $data["username"])->where('Password', $HashedPassword)->first();
            $UserDataJSON = json_encode($UserData);
            $UserData = json_decode($UserDataJSON, true);
            if($UserData != NULL && count($UserData) > 0){
                DB::table("UserDetails")
                    ->where("EmailAddress", $data["username"])
                    ->update(array("Password" => hash("SHA256", $data['newPassword'])));
                return array("status" => "success", "response" => $UserData);
            }
            return array("status" => "failed", "response" => []);
        } catch (Exception $e) {
            return array("status" => "failed", "response" => []);
        }
    }
    public function forgotPassword($data){
        try {
            $UserData = DB::table("UserDetails")->where("EmailAddress", $data["emailAddress"])->first();
            $UserDataJSON = json_encode($UserData);
            $UserData = json_decode($UserDataJSON, true);
            if($UserData != NULL && count($UserData) > 0){
                $EmailIdEncoded = base64_encode($data["emailAddress"]);
                $PasswordEncoded = base64_encode($UserData["Password"]);
                DB::table("EmailQueue")
                ->insert(array(
                    "EmailTo" => $data["emailAddress"],
                    "EmailSubject" => "Forgot Password Link",
                    "EmailBody" => "<a href='http://localhost/livepages/forgot-password/".$EmailIdEncoded."/".$PasswordEncoded."'>Forgot Password Link</a>",
                    "Status" => "",
                    "DateSent" => date("Y-m-d H:i:s"),
                    "EmailType" => "Forgot Password"
                ));
                return array("status" => "success", "response" => $UserData);
            }
            return array("status" => "failed", "response" => []);
        } catch (Exception $e) {
            return array("status" => "failed", "response" => []);
        }
    }
}