<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class ApiToken extends Model {
    public function generateToken($data){
    	try {
            $ExistingToken = DB::table("APITokenDetails")
                ->where("ClientId", $data["clientId"])
                ->where("SessionId", $data["sessionId"])
                ->where("ExpiryTime", ">", date("Y-m-d H:i:s"))
                ->get();
            $ExistingTokenJSON = json_encode($ExistingToken);
            $ExistingToken = json_decode($ExistingTokenJSON, true);
            if(count($ExistingToken) > 0){
                return array(
                    "status" => "success",
                    "response" => array(
                        "apiToken" => $ExistingToken[0]["ApiToken"],
                        "expiryTime" => $ExistingToken[0]["ExpiryTime"]
                    )
                );
            }
            
            $ApiToken = base64_encode(date("Y-m-d H:i:s"));
            $ExpiryTime = date("Y-m-d H:i:s", strtotime("+1 hour", strtotime(date("Y-m-d H:i:s"))));

            $insArr = array(
                "ClientId" => $data["clientId"],
                "SessionId" => $data["sessionId"],
                "ApiToken" => $ApiToken,
                "ExpiryTime" => $ExpiryTime
            );
            DB::table("APITokenDetails")->insert($insArr);
            return array(
                "status" => "success",
                "response" => array(
                    "apiToken" => $ApiToken,
                    "expiryTime" => $ExpiryTime
                )
            );
    	} catch (Exception $e) {
    		return array("status" => "failed", "response" => []);
    	}
    }
}
