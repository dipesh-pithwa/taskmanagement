<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Employee extends Model {
	public function __construct() {
    }

    public function saveResource($data){
        try {
            if($data["teamId"] == ""){
                // Insert
                $insArr = array(
                    "ProjectId" => $data["projectId"],
                    "EmployeeId" => $data["employeeId"],
                    "RoleId" => $data["roleId"],
                    "StartDate" => $data["startDate"],
                    "EndDate" => $data["endDate"],
                    "AllocatedHours" => $data["allocatedHours"],
                    "CreatedBy" => $data["createdBy"],
                    "ModifiedBy" => $data["modifiedBy"]
                );
                $TeamId = DB::table("ProjectTeamDetails")->insertGetId($insArr);
                return array(
                    "status" => "success",
                    "response" => "New team member added successfully",
                    "teamId" => $TeamId
                );
            }else{
                // Update
                $updArr = array(
                    "ProjectId" => $data["projectId"],
                    "EmployeeId" => $data["employeeId"],
                    "RoleId" => $data["roleId"],
                    "StartDate" => $data["startDate"],
                    "EndDate" => $data["endDate"],
                    "AllocatedHours" => $data["allocatedHours"],
                    "ModifiedBy" => $data["modifiedBy"]
                );
                DB::table("ProjectTeamDetails")->where("Id", $data["teamId"])->update($updArr);
                return array(
                    "status" => "success",
                    "response" => "New team member added successfully",
                    "teamId" => $data["teamId"]
                );
            }
        } catch (Exception $e) {
            return array(
                "status" => "failed",
                "response" => "Error",
                "teamId" => ""
            );
        }
    }

    public function getCompanies($data){
        try {
            $Response = [];
            $CompanyData = DB::table("EmployeeDetails")
                ->select("CompanyName")
                ->distinct()
                ->where("TenantId", $data["tenantId"])
                ->get();
                $CompanyDataJSON = json_encode($CompanyData);
                $CompanyData = json_decode($CompanyDataJSON, true);
                foreach ($CompanyData as $key => $value) {
                    array_push(
                        $Response,
                        $value["CompanyName"]
                    );
                }
            return array("status" => "success", "response" => $Response);
        } catch (Exception $e) {
            return array("status" => "failed", "response" => []);
        }
    }

    public function saveEmployeeLog($data){
        try {
            $insArr = array(
                "ProjectId" => $data["projectId"],
                "EmployeeId" => $data["employeeId"],
                "RoleId" => $data["roleId"],
                "LogDate" => $data["date"],
                "TimeSpent" => $data["hoursSpent"],
                "Comments" => $data["comment"]
            );
            DB::table("TimeLogEntries")->insert($insArr);
            return array("status" => "success", "response" => "Record Saved");
        } catch (Exception $e) {
            return array("status" => "failed", "response" => "");
        }
    }

    public function importEmployees($data){
        try {
            // if($data["employeecode"] == "" || $data["firstname"] == "" || $data["lastname"] == "" || $data["companyname"] == ""){
            //     return array("status" => "failed", "response" => "Employee Code, First Name, Last Name and Company Name is mandatory.");
            // }
            $Address1 = substr($data["address"], 0, 20);
            $Address2 = substr($data["address"], 20);

            // Get TenantId from Company Name.
            // $TenantData = DB::table("TenantDetails")
            //     ->where("CompanyName", $data["companyname"])
            //     ->select("Id")
            //     ->get();

            // $TenantDataJSON = json_encode($TenantData);
            // $TenantData = json_decode($TenantDataJSON, true);

            // $TenantId = "";
            // if(count($TenantData) > 0){
            //     $TenantId = $TenantData[0]["Id"];
            // }else{
            //     return array("status" => "failed", "response" => "Company Name not in Master.");
            // }

            // Get RoleId from RoleName.
            $RoleData = DB::table("SystemParams")
                ->where("ParamValue", $data["role"])
                ->select("Id")
                ->get();

            $RoleDataJSON = json_encode($RoleData);
            $RoleData = json_decode($RoleDataJSON, true);

            $RoleId = "";
            if(count($RoleData) > 0){
                $RoleId = $RoleData[0]["Id"];
            }else{
                return array("status" => "failed", "response" => "Role not in Master.");
            }

            DB::table("EmployeeDetails")
            ->updateOrInsert(
                ["EmployeeCode" => $data["employeecode"]],
                [
                    "EmployeeName" => $data["firstname"]." ".$data["lastname"],
                    "Salary" => $data["salary"],
                    "CompanyName" => $data["companyname"],
                    "TenantId" => $data["tenantId"],
                    "EmailAddress" => $data["emailaddress"],
                    "MobileNumber" => $data["mobilenumber"],
                    "Department" => $data["department"],
                    "AddressLine1" => $Address1,
                    "AddressLine2" => $Address2,
                    "City" => $data["city"],
                    "State" => $data["state"],
                    "Country" => $data["country"],
                    "DateOfJoining" => $data["dateofjoining"]
                ]
            );

            DB::table("UserDetails")
            ->updateOrInsert(
                ["EmailAddress" => $data["emailaddress"]],
                [
                    "RoleId" => $RoleId,
                    "TenantId" => $data["tenantId"],
                    "Password" => hash("SHA256", "abcdefgh"),
                    "Status" => "Active",
                    "CreatedBy" => "Admin"
                ] // $data["roleid"]]
            );

            $Email64 = base64_encode($data["emailaddress"]);
            $Password64 = base64_encode(hash("SHA256", "abcdefgh"));
            $insEmailArr = array(
                "EmailTo" => $data["emailaddress"],
                "EmailSubject" => "User On Boarding",
                "EmailBody" => "Welcome ".$data["firstname"]." ".$data["lastname"].",<br><a href='http://localhost/livepages/change-password/".$Email64."/".$Password64."'>Click here</a> to set password",
                "EmailType" => "On Boarding",
                "Status" => ""
            );
            DB::table("EmailQueue")->insert($insEmailArr);

            return array("status" => "success", "response" => "Record Saved");
        } catch (Exception $e) {
            return array("status" => "failed", "response" => "");
        }
    }

    public function updateEmployeeRole($data){
        try {
            DB::table("UserDetails")
            ->where("Id", $data["userId"])
            ->update(array("RoleId" => $data["roleId"], "Status" => $data["status"]));
            return array("status" => "success", "response" => "New user role added successfully | Employee status is updated");
        } catch (Exception $e) {
            return array("status" => "failed", "response" => "");
        }
    }

    public function getEmployeeLog($data){
    	try {
    		// $users = DB::table('users')
      //       ->join('contacts', 'users.id', '=', 'contacts.user_id')
      //       ->join('orders', 'users.id', '=', 'orders.user_id')
      //       ->select('users.*', 'contacts.phone', 'orders.price')
      //       ->get();

            // SELECT a.Id, b.ProjectId, bb.ProjectName, b.RoleId, c.ParamName as RoleName, d.LogDate, d.TimeSpent, d.Comments FROM `EmployeeDetails` a inner join ProjectTeamDetails b on a.Id = b.EmployeeId inner join ProjectDetails bb on b.ProjectId = bb.Id inner join SystemParams c on b.RoleId = c.Id inner join TimeLogEntries d on b.EmployeeId = d.EmployeeId

            $Dt = date("Y-m-d", strtotime("+1 day", strtotime($data["date"])));
            $EmployeeLog = DB::table("EmployeeDetails as a")
            ->join("ProjectTeamDetails as b", "a.Id", "=", "b.EmployeeId")
            ->join("ProjectDetails as bb", "b.ProjectId", "=", "bb.Id")
            ->join("SystemParams as c", "b.RoleId", "=", "c.Id")
            ->join("TimeLogEntries as d", "b.EmployeeId", "=", "d.EmployeeId")
            ->select("a.Id", "b.ProjectId", "bb.ProjectName", "b.RoleId", "c.ParamName as RoleName", "d.LogDate", "d.TimeSpent", "d.Comments")
            ->where("b.EmployeeId", $data["employeeId"])
            ->where("d.LogDate", ">=", $data["date"])
            ->where("d.LogDate", "<=", $Dt)
            ->get();
    		return array("status" => "success", "response" => $EmployeeLog);
        } catch (Exception $e) {
            return array("status" => "failed", "response" => []);
        }	
    }
    public function getEmployee($data){
        try {
            $Response = [];
            $EmployeeData = [];
            if($data["pageSize"] == 0 && $data["pageNumber"] == 0){
                $EmployeeData = DB::table("EmployeeDetails as e")
                ->join("UserDetails as u", "e.EmailAddress", "=", "u.EmailAddress")
                ->join("SystemParams as s", "u.RoleId", "=", "s.Id")
                ->select("e.Id", "e.EmployeeCode", "e.EmployeeName", "s.ParamValue", "e.CompanyName", "e.Department", "u.Status")
                ->orderBy("e.EmployeeName", "asc")
                ->get();
                $EmployeeDataJSON = json_encode($EmployeeData);
                $EmployeeData = json_decode($EmployeeDataJSON, true);
                $EmployeeDataCount = count($EmployeeData);
                foreach ($EmployeeData as $key => $value) {
                    array_push(
                        $Response,
                        array(
                            "id" => $value["Id"],
                            "employeeCode" => $value["EmployeeCode"],
                            "employeeName" => $value["EmployeeName"],
                            "roleName" => $value["ParamValue"],
                            "companyName" => $value["CompanyName"],
                            "department" => $value["Department"],
                            "status" => $value["Status"]
                        )
                    );
                }
                return array("status" => "success", "response" => $Response, "totalPages" => $EmployeeDataCount);
            }
            if(strlen($data["employeeName"]) >= 3 && $data["employeeName"] != ""){
                $EmployeeDataCount = DB::table("EmployeeDetails as e")
                ->where("e.EmployeeName", "like", $data["employeeName"]."%")
                ->join("UserDetails as u", "e.EmailAddress", "=", "u.EmailAddress")
                ->join("SystemParams as s", "u.RoleId", "=", "s.Id")
                ->count();
                $TotalPageCount = ceil($EmployeeDataCount / $data["pageSize"]);

                $Offset = ($data["pageNumber"] * $data["pageSize"]) - $data["pageSize"];
                $EmployeeData = DB::table("EmployeeDetails as e")
                ->where("e.EmployeeName", "like", $data["employeeName"]."%")
                ->join("UserDetails as u", "e.EmailAddress", "=", "u.EmailAddress")
                ->join("SystemParams as s", "u.RoleId", "=", "s.Id")
                ->select("e.Id", "e.EmployeeCode", "e.EmployeeName", "s.ParamValue", "e.CompanyName", "e.Department", "u.Status")
                ->orderBy("e.EmployeeName", "asc")
                ->offset($Offset)
                ->limit($data["pageSize"])
                ->get();
                $EmployeeDataJSON = json_encode($EmployeeData);
                $EmployeeData = json_decode($EmployeeDataJSON, true);
                foreach ($EmployeeData as $key => $value) {
                    array_push(
                        $Response,
                        array(
                            "id" => $value["Id"],
                            "employeeCode" => $value["EmployeeCode"],
                            "employeeName" => $value["EmployeeName"],
                            "roleName" => $value["ParamValue"],
                            "companyName" => $value["CompanyName"],
                            "department" => $value["Department"],
                            "status" => $value["Status"]
                        )
                    );
                }
            }elseif($data["employeeName"] == ""){

                $EmployeeDataCount = DB::table("EmployeeDetails as e")
                ->join("UserDetails as u", "e.EmailAddress", "=", "u.EmailAddress")
                ->join("SystemParams as s", "u.RoleId", "=", "s.Id")
                ->count();

                $TotalPageCount = ceil($EmployeeDataCount / $data["pageSize"]);

                $Offset = ($data["pageNumber"] * $data["pageSize"]) - $data["pageSize"];
                $EmployeeData = DB::table("EmployeeDetails as e")
                ->join("UserDetails as u", "e.EmailAddress", "=", "u.EmailAddress")
                ->join("SystemParams as s", "u.RoleId", "=", "s.Id")
                ->select("e.Id", "e.EmployeeCode", "e.EmployeeName", "s.ParamValue", "e.CompanyName", "e.Department", "u.Status")
                ->orderBy("e.EmployeeName", "asc")
                ->offset($Offset)
                ->limit($data["pageSize"])
                ->get();
                $EmployeeDataJSON = json_encode($EmployeeData);
                $EmployeeData = json_decode($EmployeeDataJSON, true);
                foreach ($EmployeeData as $key => $value) {
                    array_push(
                        $Response,
                        array(
                            "id" => $value["Id"],
                            "employeeCode" => $value["EmployeeCode"],
                            "employeeName" => $value["EmployeeName"],
                            "roleName" => $value["ParamValue"],
                            "companyName" => $value["CompanyName"],
                            "department" => $value["Department"],
                            "status" => $value["Status"]
                        )
                    );
                }
            }
            return array("status" => "success", "response" => $Response, "totalPages" => $TotalPageCount);
        } catch (Exception $e) {
            return array("status" => "failed", "response" => [], "totalPages" => 0);
        }
    }
    public function getEmployeeById($data){
        try {
            $Response = [];
            $EmployeeData = DB::table("EmployeeDetails as e")
                ->where("e.Id", $data["employeeId"])
                ->join("UserDetails as u", "e.EmailAddress", "=", "u.EmailAddress")
                ->join("SystemParams as s", "u.RoleId", "=", "s.Id")
                ->select("e.Id", "u.Id as UserId", "e.EmployeeCode", "e.EmployeeName", "s.ParamValue", "e.CompanyName", "e.Department", "u.Status", "e.TenantId", "u.RoleId")
                ->get();
                $EmployeeDataJSON = json_encode($EmployeeData);
                $EmployeeData = json_decode($EmployeeDataJSON, true);
                foreach ($EmployeeData as $key => $value) {
                    array_push(
                        $Response,
                        array(
                            "id" => $value["Id"],
                            "employeeCode" => $value["EmployeeCode"],
                            "employeeName" => $value["EmployeeName"],
                            "roleName" => $value["ParamValue"],
                            "companyName" => $value["CompanyName"],
                            "department" => $value["Department"],
                            "status" => $value["Status"],
                            "tenantId" => $value["TenantId"],
                            "userId" => $value["UserId"],
                            "roleId" => $value["RoleId"]
                        )
                    );
                }
            return array("status" => "success", "response" => $Response);
        } catch (Exception $e) {
            return array("status" => "failed", "response" => []);
        }
    }
    public function getEmployeeProjects($data){
        try {
            $Response = [];
            $EmployeeProjects = DB::table("ProjectTeamDetails as t")
                ->join("ProjectDetails as p", "t.ProjectId", "=", "p.Id")
                ->join("SystemParams as s", "t.RoleId", "=", "s.Id")
                ->where("EmployeeId", $data["employeeId"])
                ->select("p.Id", "p.Projectname", "s.ParamValue")
                ->get();
            $EmployeeProjectsJSON = json_encode($EmployeeProjects);
            $EmployeeProjects = json_decode($EmployeeProjectsJSON, true);
            foreach ($EmployeeProjects as $key => $value) {
                array_push(
                    $Response,
                    array(
                        "projectId" => $value["Id"],
                        "projectName" => $value["Projectname"],
                        "roleName" => $value["ParamValue"]
                    )
                );
            }
            return array("status" => "success", "response" => $Response);
        } catch (Exception $e) {
            return array("status" => "failed", "response" => []);
        }
    }
}