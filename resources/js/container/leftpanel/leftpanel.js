import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import Api from '../../Api';

const leftpanel=React.memo(function MyComponent(props){
	let role = document.getElementById("role").value;
	let currURL = location.href;
	let currActive = "";
	if(currURL.indexOf("Configuration") != -1){
		currActive = "Config";
	}else if(currURL.indexOf("LogHistory") != -1){
		currActive = "LogHist";
	}else if(currURL.indexOf("Logtime") != -1){
		currActive = "Log";
	}else if(currURL.indexOf("ProjectMng") != -1){
		currActive = "Proj";
	}else{
		currActive = "Emp";
	}

	const [active, setActive] = useState(currActive);
    return (
        <aside className="main-sidebar">
			
			<section className="sidebar">
				
				<ul className="sidebar-menu" data-widget="tree">
				{
					(role == "System Admin") &&
					<li className={active == "Emp"?"active":""}>
						<Link to={Api.baseurl} onClick={() => setActive("Emp")}>
						<img src={Api.baseurl+'public/images/empmangment-icon.png'} className="ic-mgt" alt="empmangment"/><span>Employee Management</span>
						</Link>
					</li>
				}
					{
						role == "System Admin" && 
						<li className={active == "Config"?"active":""}>
							<Link to={Api.baseurl+"Configuration"} onClick={() => setActive("Config")}>
								<img src={Api.baseurl+'public/images/config-mgm-icon.png'} alt="Configuration" className="ic-mgt" /><span>Configuration Management</span>
							</Link>
						</li>
					}
					{
						(role == "System Admin" || role == "Project Manager") &&
						<li className={active == "Proj"?"active":""}>
							<Link to={Api.baseurl+"ProjectMng"} onClick={() => setActive("Proj")}>
							<img src={Api.baseurl+'public/images/promgmt-ic.png'} className="ic-mgt" alt="imagespromgmt"/><span>Project Management </span>
							</Link>
						</li>
					}
					
					{
						role != "System Admin" && 
						<li className={active == "Log"?"active":""}>
							<Link to={Api.baseurl+"Logtime"} onClick={() => setActive("Log")}>
							<img src={Api.baseurl+'public/images/log-time-bg.png'} className="ic-mgt" alt="log time"/><span>Log Time</span>
							</Link>
						</li>
					}
					{
						role != "System Admin" && 
						<li className={active == "LogHist"?"active":""}>
							<Link to={Api.baseurl+"LogHistory"} onClick={() => setActive("LogHist")}>
							 <img src={Api.baseurl+'public/images/log-time-history-bg.png'} className="ic-mgt" alt="log time" /><span>View Log Time History</span>
							</Link>
						</li>
					}
				</ul>
			</section>
		
		</aside>
    )
});

export default leftpanel;