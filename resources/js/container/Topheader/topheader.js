import React, { useState, Component } from 'react';
import './topheader.css';
import {Link} from 'react-router-dom';
import Api from '../../Api';

// export default function topheader(props) {
class topheader extends Component {
    constructor(props){
        super(props);
        // const [count, setCount] = useState(false);
        this.state = {
            count: false
        };
        this.redirectLogout = this.redirectLogout.bind(this);
        this.redirectChangePassword = this.redirectChangePassword.bind(this);
    }

    redirectLogout(){
        location.href = Api.baseurl+"logout";
    }

    redirectChangePassword(){
        let EmailId = document.getElementById("emailId").value;
        EmailId = btoa(EmailId);
        let secret = document.getElementById("secret").value;
        window.open(Api.baseurl+"change-password/"+EmailId+"/"+secret, "_blank");
    }

    render(){
        let firstName = document.getElementById("firstName").value;
        let logout;
        let changePassword;
        if(this.state.count == true){
            logout = <ul className="dropdown-menu" style={{"display": "block"}}><li className="user-footer"><div className="pull-left"><button onClick={this.redirectLogout}>Sign Out</button></div></li><li className="user-footer"><div className="pull-left"><button onClick={this.redirectChangePassword}>Change Password</button></div></li></ul>;
        }else{
            logout = logout = <ul className="dropdown-menu" style={{"display": "none"}}><li className="user-footer"><div className="pull-left"><button onClick={this.redirectLogout}>Sign Out</button></div></li><li className="user-footer"><div className="pull-left"><button onClick={this.redirectChangePassword}>Change Password</button></div></li></ul>;
        }

        return (
            <header className="main-header">
           
            <Link to={Api.baseurl} className="logo">
               
                <span className="logo-lg"><img width="20%" src={Api.baseurl+'public/images/logo.jpg'} alt="logo"/></span>
            </Link>
         
            <nav className="navbar navbar-static-top">
                <div className="navbar-custom-menu">
                    <ul className="nav navbar-nav">
                        <li onClick={() => this.setState({count: !this.state.count})} className="dropdown user user-menu">
                            <Link to="#" className="dropdown-toggle" data-toggle="dropdown">
                            <span className="hidden-xs">{firstName}</span>
                            </Link>
                            {logout}
                        </li>
                    </ul>
                </div>
            </nav>
        </header> 
        )
    }
}
export default topheader;