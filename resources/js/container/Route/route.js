import React, { Component } from 'react'
import Home from '../../component/Home/home';
import { Route,Switch } from 'react-router-dom'
import Editemployee from '../../component/editemployee/editemployee';
import Configuration from '../../component/Configuration/Configuration';
import LogHistory from '../../component/LogHistory/loghistory';
import Logtime from '../../component/Logtime/logtime';
import Api from '../../Api';
import ProjectMng from '../../component/Projectmng/projectmng';
import Projectmngtab from '../../component/Projectmng/tabview';

 class routeMain extends Component {
  render() {
    let role = document.getElementById("role").value;
    return (
      <div>
        <Switch>
            <Route path={Api.baseurl} exact component={Home}/>
            <Route path={Api.baseurl+"Editemployee/:id"} component={Editemployee}/>
            <Route path={Api.baseurl+"Configuration"} component={Configuration}/>
            <Route path={Api.baseurl+"LogHistory"} component={LogHistory}/>
            <Route path={Api.baseurl+"Logtime"} component={Logtime}/>
            <Route path={Api.baseurl+"ProjectMng"} component={ProjectMng}/>
            <Route path={Api.baseurl+"Projectmngtab/:id"} component={Projectmngtab}/>
        </Switch>
      </div>
    )
  }
}
export default routeMain;
