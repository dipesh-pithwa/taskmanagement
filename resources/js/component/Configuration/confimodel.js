import React,{Component} from 'react';
import HOCmodel from '../modelfileHoc';
import Api from '../../Api';



class confimodel extends Component{
    constructor(props){
        super(props);
        this.state = {
            tabactive:'tab1',
            paramList: [],
            departments: [],
            subParams: [],
            allParams: [],
            apiMessage: "",
            style: "",
            prefillDisplayName: "",
            prefillValue: "",
            prefillOrder: "",
            prefillDescription: "",
            prefillStatus: ""
        };
        this.handleChangeDept = this.handleChangeDept.bind(this);
        this.handleChangeParam = this.handleChangeParam.bind(this);
        this.saveParam = this.saveParam.bind(this);

        this.changeDisplayName = this.changeDisplayName.bind(this);
        this.changeValue = this.changeValue.bind(this);
        this.changeOrder = this.changeOrder.bind(this);
        this.changeDescription = this.changeDescription.bind(this);
        this.changeStatus = this.changeStatus.bind(this);

        this.parentId = React.createRef();
        this.paramName = React.createRef();
        this.paramValue = React.createRef();
        this.order = React.createRef();
        this.description = React.createRef();
        this.status = React.createRef();

        console.log("ConfigData:  "+props.configData);
    }

    componentDidMount(){
        let tenantId = document.getElementById("tenantId").value;
        let Data = { "tenantId" : tenantId};
        let URL = Api.endpoint+'getParamList';
        fetch(URL, {
          method: 'post',
          body: JSON.stringify(Data)
        })
        .then(response => response.json())
        .then(data => {
                this.setState({
                    paramList: data.params,
                    departments: data.params[0].departments,
                    // subParams: data.params[0].departments[0].params
                    subParams: data.allParams,
                    allParams: data.allParams
                });
                console.log("ParamName:  "+this.props.configData);
                if(this.props.configData != ""){
                  let URL2 = Api.endpoint+'getSelectedParamDetails';
                  let Data2 = { "tenantId" : tenantId, "paramName": this.props.configData};
                  fetch(URL2, {
                    method: 'post',
                    body: JSON.stringify(Data2)
                  })
                  .then(response2 => response2.json())
                  .then(data2 => {
                    // Prefill data from here.
                    console.log(data2);
                    this.setState({
                      prefillDisplayName: data2.params[0].paramName,
                      prefillValue: data2.params[0].paramValue,
                      prefillOrder: data2.params[0].orderCount,
                      prefillDescription: data2.params[0].description,
                      prefillStatus: data2.params[0].status
                    });
                  });
                }
            }
        );
    }
    
     tabactivehandler(data){
        this.setState({
            tabactive:data
        })
     }

    handleChangeDept(e){
        let dept = e.target.value;
        if(dept != ""){
          let paramList = this.state.paramList;
          paramList[0].departments.map(hit1 => {
                  if(hit1.departmentName == dept){
                      this.setState({
                          subParams: hit1.params
                      });
                  }
              }
          );
        }else{
          let allParams = this.state.allParams;
          this.setState({
            subParams: allParams
          });
        }
    }

    handleChangeParam(e){
        let param = e.target.value;
        // showParams
        let tenantId = document.getElementById("tenantId").value;
        let Data = { "tenantId" : tenantId, "paramName": param};
        let URL = Api.endpoint+'getParamDetails';
        fetch(URL, {
          method: 'post',
          body: JSON.stringify(Data)
        })
        .then(response => response.json())
        .then(data => {
                this.setState({
                    showParams: data.params
                });
            }
        );
    }

    saveParam(){
      let tenantId = document.getElementById("tenantId").value;
      if(this.paramName.current.value == ""){
        this.setState({
          apiMessage: "Param name cannot be blank."
        });
      }else if(this.paramValue.current.value == ""){
        this.setState({
          apiMessage: "Param value cannot be blank."
        });
      }else if(this.order.current.value == ""){
        this.setState({
          apiMessage: "Order cannot be blank."
        });
      }else if(this.status.current.value == ""){
        this.setState({
          apiMessage: "Status cannot be blank."
        });
      }else{
        let paramId = this.props.configId;
        let Data = {"tenantId": tenantId, "paramId": paramId, "paramName": this.paramName.current.value, "paramValue": this.paramValue.current.value, "parentId": this.parentId.current.value, "name": this.description.current.value, "status": this.status.current.value, "order": this.order.current.value};
        let URL = Api.endpoint+'saveParameter';
        fetch(URL, {
          method: 'post',
          body: JSON.stringify(Data)
        })
        .then(response => response.json())
        .then(data => {
                if(data.result == "success"){
                  this.setState({
                    // style: "background-color: green; color: white",
                    apiMessage: "New parameter added / updated successfully."
                  });
                }else{
                  this.setState({
                    // style: "background-color: red; color: white",
                    apiMessage: "Error adding parameter.."
                  });
                }
            }
        );
      }
    }

    changeDisplayName(e){
      let val = e.target.value;
      this.setState({
        prefillDisplayName: val
      });
    }
    changeValue(e){
      let val = e.target.value;
      this.setState({
            prefillValue: val
      });
    }
    changeOrder(e){
      let val = e.target.value;
      if(val >= 0 && val != "-0"){
        this.setState({
            prefillOrder: val
      });
      }
    }
    changeDescription(e){
      let val = e.target.value;
      this.setState({
            prefillDescription: val
      });
    }
    changeStatus(e){
      let val = e.target.value;
      this.setState({
            prefillStatus: val
      });
    }
    
    render(){
      const {paramList} = this.state;
      const {departments} = this.state;
      const {subParams} = this.state;
      console.log(this.props.deptData);
         return(
            <HOCmodel saveData={this.saveParam} title="Add / Edit Configuration Parameter" closemodel={this.props.closemodel}>
                <div className="row">
                <div className="col-sm-6">
                 <div className="form-group">
                  <label>Company Name</label>
                  <select name="company_name" className="form-control" disabled="true">
                     {
                        paramList.map(hit => 
                            <option value={hit.companyId}>{hit.companyName}</option>
                        )
                      }
                  </select>
                 </div>
                </div>
                <div className="col-sm-6">
                 <div className="form-group">
                  <label>Department</label>
                  <select name="Department" className="form-control" onChange={this.handleChangeDept} disabled={this.props.deptData != ""}>
                    <option value="">--- Select ---</option>
                     {
                        departments.map(departmentshit =>
                            <option value={departmentshit.departmentName} selected={this.props.deptData == departmentshit.departmentName}>{departmentshit.departmentName}</option>
                        )
                      }
                  </select>
                 </div>
                </div>
                <div className="col-sm-6">
                 <div className="form-group">
                  <label>Parameter</label>
                  <select name="parameter" className="form-control" onChange={this.handleChangeParam} ref={this.parentId}>
                     {
                        subParams.map(subParamshit =>
                            <option value={subParamshit.paramId} selected={this.props.paramData == subParamshit.paramName}>{subParamshit.paramName}</option>
                        )
                      }
                  </select>
                 </div>
                </div>
                <div className="col-sm-6">
                 <div className="form-group">
                  <label>Display Name</label>
                    <input type="text" className="form-control" name="Name" required="" ref={this.paramName} value={this.state.prefillDisplayName} onChange={(e)=>{this.changeDisplayName(e)}} />
                 </div>
                </div>
                <div className="col-sm-6">
                 <div className="form-group">
                  <label>Value</label>
                    <input type="text" className="form-control" name="Value" placeholder="" required="" ref={this.paramValue} value={this.state.prefillValue} onChange={(e)=>{this.changeValue(e)}} />
                 </div>
                </div>
                <div className="col-sm-6">
                 <div className="form-group">
                  <label>Order</label>
                    <input type="number" className="form-control" name="Order" placeholder="" required="" ref={this.order} value={this.state.prefillOrder} onChange={(e)=>{this.changeOrder(e)}} />
                 </div>
                </div>
                <div className="col-xs-12">
                 <div className="form-group">
                  <label>Description (Optional)</label>
                    <textarea className="form-control" row="3" ref={this.description} value={this.state.prefillDescription} onChange={(e)=>{this.changeDescription(e)}}></textarea>
                 </div>
                </div>
                <div className="col-sm-6">
                 <div className="form-group">
                  <label>Status</label>
                    <select name="parameter" className="form-control" ref={this.status} value={this.state.prefillStatus} onChange={(e)=>{this.changeStatus(e)}}>
                      <option value="">Select</option>
                      <option value="Active">Active</option>
                      <option value="Inactive">Inactive</option>
                    </select>
                 </div>
                </div>
                <div className="col-xs-12">
                 <div className="form-group">
                  <p>{this.state.apiMessage}</p>
                 </div>
                </div>
               </div>
            </HOCmodel>  
            
        )
    }
   
   
}

export default confimodel;