import React,{Component} from 'react';
import {Link} from 'react-router-dom';
import ConfiModel from './confimodel';
import Api from '../../Api';

class Configuration extends Component{
    constructor(props){
        super(props);
            this.state={
                show:false,
                addclass:null,
                allParams: [],
                paramList: [],
                departments: [],
                subParams: [],
                selectedCompany: "",
                selectedDepartment: "",
                selectedParam: "",
                showParams: [],
                company: [],
                editConfigData: ""
        }
        //this.handleshow=this.handleshow.bind(this);
       this.handlehide=this.handlehide.bind(this);
       this.handleChangeDept = this.handleChangeDept.bind(this);
       this.handleChangeParam = this.handleChangeParam.bind(this);

       this.Dept = React.createRef();
    }
   
    componentDidMount(){
        let tenantId = document.getElementById("tenantId").value;
        let Data = { "tenantId" : tenantId};
        let URL = Api.endpoint+'getParamList';
        fetch(URL, {
          method: 'post',
          body: JSON.stringify(Data)
        })
        .then(response => response.json())
        .then(data => {
                let companyArr = new Array();
                for(let i = 0; i < data.params.length; i++){
                    companyArr[i] = {
                        "companyId": data.params[i].companyId,
                        "companyName": data.params[i].companyName
                    };
                }
                this.setState({
                    allParams: data.params,
                    paramList: companyArr,
                    departments: data.params[0].departments
                });
            }
        );

        let Data2 = { "tenantId" : tenantId};
        let URL2 = Api.endpoint+'getCompanies';
        fetch(URL2, {
          method: 'post',
          body: JSON.stringify(Data2)
        })
        .then(response => response.json())
        .then(data => {
                this.setState({
                    company: data.result
                });
            }
        );
    }
    
    handleshow(configParamName, paramId){
        this.setState({
            show:true,
            addclass:'in',
            editConfigData: configParamName,
            editConfigId: paramId
        })
    }
    handlehide(){
        this.setState({
            show:false,
            addclass:'',
            editConfigData: "",
            editConfigId: ""
        })
    }

    handleChangeDept(e){
        let dept = e.target.value;
        if(dept == ""){
            this.setState({
                showParams: [],
                subParams: []
            });
        }else{
            this.setState({
                selectedDepartment: dept
            });
            let allParams = this.state.allParams;
            allParams[0].departments.map(hit1 => {
                    if(hit1.departmentName == dept){
                        this.setState({
                            subParams: hit1.params
                        });
                    }
                }
            );
        }
    }

    handleChangeParam(e){
        let param = e.target.value;
        // showParams
        this.setState({
            selectedParam: param
        });
        let tenantId = document.getElementById("tenantId").value;
        let Data = { "tenantId" : tenantId, "paramName": param};
        let URL = Api.endpoint+'getParamDetails';
        fetch(URL, {
          method: 'post',
          body: JSON.stringify(Data)
        })
        .then(response => response.json())
        .then(data => {
                this.setState({
                    showParams: data.params
                });
            }
        );
    }
    
    render(){
        const {paramList} = this.state;
        const {departments} = this.state;
        const {subParams} = this.state;
        const {showParams} = this.state;
        const {company} = this.state;
        let srno = 1;
        console.log(paramList);
        return(
              <React.Fragment>
                <section className="content-header">
                    <ol className="breadcrumb">
                    <li><Link to="/">Home</Link></li>
                    <li className="active" >Configuration Parameter Management</li>
                    </ol>
                </section>
                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="addnew-parameter">
                                <button type="button" className="btn btn-default fix-button" onClick={(e)=>this.handleshow("")}>Add New Parameter</button>
                            </div>  
                            <div className="box">
                                <div className="para-mg-select">
                                    <div className="row">
                                        <div className="col-sm-4">
                                            <label>Company Name</label>
                                            <select name="company_name" className="form-control">
                                            {
                                                paramList.map(hit => 
                                                    <option value={hit.companyId}>{hit.companyName}</option>
                                                )
                                            }
                                            </select>
                                        </div>
                                        <div className="col-sm-4">
                                            <label>Department</label>
                                            <select name="Department" className="form-control" onChange={this.handleChangeDept} ref={this.Dept}>
                                                <option value="">--- Select ---</option>
                                            {
                                                departments.map(departmentshit =>
                                                    <option value={departmentshit.departmentName}>{departmentshit.departmentName}</option>
                                                )
                                            }
                                            </select>
                                        </div>
                                        <div className="col-sm-4">
                                            <label>Parameter</label>
                                            <select name="parameter" className="form-control" onChange={this.handleChangeParam}>
                                                <option value="">--- Select ---</option>
                                            {
                                                subParams.map(subParamshit =>
                                                    <option value={subParamshit.paramName}>{subParamshit.paramName}</option>
                                                )
                                            }
                                            </select>
                                        </div>
                                    </div>
                                    </div>
                                    {/* end body */}
                                 <div className="box-body disnon">
                                    <h4 className="table-heading">Parameter Management</h4>
                                    <table id="example1" className="table table-bordered table-striped table-responsive">
                                        <thead>
                                            <tr>
                                                <th>Sr no</th>
                                                <th>Display Name</th>
                                                <th>Parent Name </th>
                                                <th>Status</th>
                                                <th>Description</th>
                                                <th>Order</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                showParams.map(hit3 => 
                                                        <tr key={hit3.id}>
                                                            <td>{srno++}</td>
                                                            <td>{hit3.paramValue}</td>
                                                            <td>{hit3.paramName}</td>
                                                            <td>{hit3.status}</td>
                                                            <td>{hit3.description}</td>
                                                            <td>{hit3.orderCount}</td>
                                                            <td><button type="button" onClick={(e)=>this.handleshow(hit3.paramName, hit3.id)} className="edt-btn" data-toggle="modal" data-target=".bs-example-modal-lg"><img src={Api.baseurl+'public/images/edit-bg.png'} alt="edit" /></button></td>
                                                        </tr>
                                                    
                                                )
                                            }
                                        </tbody>
                                    </table>
                                 </div>
                                    {/*box-body  */}
                            </div> 
                        </div>
                    </div>
                </section>
                 {this.state.show===true?<ConfiModel deptData={this.Dept.current.value} paramData={this.state.selectedParam} configData={this.state.editConfigData} configId={this.state.editConfigId} closemodel={this.handlehide}/>:null}   
              
              </React.Fragment>  
        )
    }
}

export default Configuration;