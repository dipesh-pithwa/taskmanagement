import React,{Component} from 'react'
import {Link} from 'react-router-dom';
import api from '../../Api.js';

class editemployee extends Component{
    constructor(props){
        super(props);
        this.state = {
          emp: [
              {
                    "employeeCode": "",
                    "employeeName": "",
                    "status": "",
                    "roleName": ""
              }
          ],
          role: [],
          selectedRole: "",
          selectedEmpStatus: "",
          editMessage: "",
          isRoleChanged: false,
          isStatusChanged: false
        };
        this.callGetEmployee = this.callGetEmployee.bind(this);
        this.callGetRoles = this.callGetRoles.bind(this);
        this.handleSave = this.handleSave.bind(this);
        this.handleChangeRole = this.handleChangeRole.bind(this);
        this.handleChangeStatus = this.handleChangeStatus.bind(this);

        this.userStatus = React.createRef();
        this.userRole = React.createRef();
    }
    callGetEmployee(){
        let Data = {"employeeId": this.props.match.params.id};
        let URL = api.endpoint+'getEmployeeById';
        fetch(URL, {
          method: 'post',
          body: JSON.stringify(Data)
        })
        .then(response => response.json())
        .then(data => {
            this.setState({ 
                emp: data.employees,
                selectedRole: data.employees[0].roleId,
                selectedEmpStatus: data.employees[0].status
            });
            this.callGetRoles(data.employees[0].tenantId);
        });
    }
    callGetRoles(tenantId){
        let Data = {"tenantId": tenantId};
        let URL = api.endpoint+'getRoles';
        fetch(URL, {
          method: 'post',
          body: JSON.stringify(Data)
        })
        .then(response => response.json())
        .then(data => {
            this.setState({ role: data.roles });
        });
    }
    handleSave(){
        let Data = { "tenantId": this.state.emp[0].tenantId, "userId": this.state.emp[0].userId, "roleId": this.userRole.current.value, "operation" : "RoleUpdate", "status": this.userStatus.current.value };
        let URL = api.endpoint+'updateEmployeeRole';
        fetch(URL, {
          method: 'post',
          body: JSON.stringify(Data)
        })
        .then(response => response.json())
        .then(data => {
            if(data.code == 200){
                if(this.state.isRoleChanged == true && this.state.isStatusChanged == false){
                    this.setState({
                        editMessage: "New user role updated successfully",
                        isRoleChanged: false,
                        isStatusChanged: false
                    });
                }else if(this.state.isRoleChanged == false && this.state.isStatusChanged == true){
                    this.setState({
                        editMessage: "Employee status is updated",
                        isRoleChanged: false,
                        isStatusChanged: false
                    });
                }else{
                    this.setState({
                        editMessage: "New user role updated successfully | Employee status is updated",
                        isRoleChanged: false,
                        isStatusChanged: false
                    });
                }
                
                setTimeout(function(){
                    this.setState({
                        editMessage: ""
                    });
                }.bind(this), 5000);
            }else{
                this.setState({
                    editMessage: data.response
                });
                setTimeout(function(){
                    this.setState({
                        editMessage: ""
                    });
                }.bind(this), 5000);
            }
        });
    }
    handleChangeRole(e){
        this.setState({
            "selectedRole": e.target.value,
            "isRoleChanged": true
        });
    }
    handleChangeStatus(e){
        this.setState({
            "selectedEmpStatus": e.target.value,
            "isStatusChanged": true
        });
    }
    componentDidMount(){
        this.callGetEmployee();
    }
    render(){
        const {role} = this.state;
        return(
            <React.Fragment>
                <section className="content-header">
                    <ol className="breadcrumb">
                     <li><Link to={api.baseurl}>Home</Link></li>
                     <li>Employee Management</li>
                     <li className="active">Edit Employee</li>
                    </ol>
                </section>
                <section className="content">
                    <h4 className="table-heading">Edit Employee</h4>
                     <div className="addemp">
                        <form>
                            <div className="row mgbtm">
                                <div className="col-sm-3">
                                <label>Employee Code </label>
                                <input type="text" className="form-control" name="code" disabled="disabled" value={this.state.emp[0].employeeCode}/>
                                </div>
                                <div className="col-sm-3">
                                <label>Employee Name </label>
                                <input type="text" className="form-control" name="name" disabled="disabled" value={this.state.emp[0].employeeName}/>
                                </div>
                                <div className="col-sm-3">
                                <label>Status</label>
                                <select name="status" className="form-control" onChange={this.handleChangeStatus} value={this.state.selectedEmpStatus} ref={this.userStatus}>
                                <option value="Active">Active</option>
                                <option value="Inactive">Inactive</option>
                                </select>
                                </div>
                                <div className="col-sm-3">
                                <label>Role</label>
                                <select name="role" className="form-control" onChange={this.handleChangeRole} value={this.state.selectedRole} ref={this.userRole}>
                                    {
                                        role.map(hit => 
                                            <option value={hit.id} key={hit.id}>{hit.paramValue}</option>
                                        )
                                    }
                                </select>
                                </div>

                            </div>

                            <div className="row">
                                <div className="col-sm-12">
                                    <p>{this.state.editMessage}</p>
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-sm-12">
                                <button style={{marginRight:'10px'}} type="button" className="btn btn-default fix-button" onClick={this.handleSave}>Save</button>
                                <Link to={api.baseurl} className="btn btn-default fix-button clr-new">Back</Link>
                                </div>
                            </div>
                        </form>
                     </div>
                </section>
            </React.Fragment>
        )
    }
} 

export default editemployee;