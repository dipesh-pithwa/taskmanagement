import React, { Component } from 'react'
import {Link} from 'react-router-dom';
import Api from '../../Api';

class home extends Component {
	constructor(props){
		super(props);
	    this.state = {
	      emp: [],
	      messageView: false,
	      totalRecords: "",
	      successRecords: "",
	      failedRecords: "",
	      failureResponse: [],
	      currPageNo: 1,
	      srcStr: "",
	      totalPages: 0,
	      showNext: true,
	      showPrevious: false,
	      errMsg: "",
	      allPageNo: [],
	      loaderText: ""
	    };
	    this.fileInput = React.createRef();
	    this.callGetEmployee = this.callGetEmployee.bind(this);
	    this.searchEmployee = this.searchEmployee.bind(this);
	    this.handleSubmit = this.handleSubmit.bind(this);
	    this.handleNext = this.handleNext.bind(this);
	    this.handlePrevious = this.handlePrevious.bind(this);
	    this.handleChangeSrcStr = this.handleChangeSrcStr.bind(this);
	    this.handleFirst = this.handleFirst.bind(this);
	    this.handleLast = this.handleLast.bind(this);
	    this.gotoPage = this.gotoPage.bind(this);
	}
	callGetEmployee(pageNo, empName){
		let Data = {"employeeName":empName,"pageSize":"10","pageNumber":pageNo};
	    let URL = Api.endpoint+'getEmployees';
	    fetch(URL, {
	      method: 'post',
	      body: JSON.stringify(Data)
	    })
	    .then(response => response.json())
	    .then(data => {
	    	this.setState({ emp: data.employees, totalPages: data.totalPages });
	    	let pageArr = new Array();
	    	for(let i = 1; i <= data.totalPages; i++){
	    		pageArr[i - 1] = {value: i};
	    	}
	    	this.setState({
	    		allPageNo: pageArr
	    	});
	    	console.log(this.state.allPageNo);
	    	console.log("Total Pages:  "+data.totalPages);
	    	if(data.totalPages == 1 || pageNo == data.totalPages || data.totalPages == 0){
	    		this.setState({showNext: false});
	    	}else if(data.totalPages > 1){
	    		this.setState({showNext: true});
	    	}
	    });
	}
	handleSubmit(e){
		let tenantId = document.getElementById("tenantId").value;
	    let file = e.target.files[0];
	    let fileName = file.name;
	    let fileSize = (file.size/1000)/1000;
	    let this2 = this;
	    if(fileName.slice(-5) == ".xlsx" && fileSize < 2){
	    	this.setState({
		      	errMsg: "",
		      	loaderText: "Data import in progress..."
		    });
	    	const formData = new FormData();
		    formData.append('importEmployees', file);
		    formData.append('tenantId', tenantId);
		    fetch(Api.endpoint+'importEmployees', {
		      method: 'POST',
		      body: formData
		    }).then(
		      response => response.json()
		    ).then(
		      success => {
		      	this.setState({
		      		totalRecords: success.totalRecordCount,
					successRecords: success.successRecordCount,
					failedRecords: success.failedRecordCount,
					failureResponse: success.failedRows,
		      		messageView: true,
		      		loaderText: ""
		      	});
		      	setTimeout(function(){
		      		this2.setState({
		      			messageView: false
		      		});
		    //   		this.setState({
			   //    		totalRecords: "",
						// successRecords: "",
						// failedRecords: "",
						// failureResponse: [],
						// messageView: false,
		    //   		});
		      	}.bind(this), 5000);
		      }
		    ).catch(
		      error => console.log(error)
		    );
	    }else{
	    	this.setState({
		      	errMsg: "File should be .xlsx format & less than 2 MB.",
		    });
	    }
	}
	handleChangeSrcStr(e){
		this.setState({
			srcStr: e.target.value
		});
	}
	searchEmployee(e){
		if(e.keyCode == 13){
			let searchStr = this.state.srcStr;
			searchStr = searchStr.trim();
			if(searchStr != "" && searchStr.length > 2){
				this.callGetEmployee(this.state.currPageNo, searchStr);
			}else if(searchStr == ""){
				this.callGetEmployee(this.state.currPageNo, "");
			}
		}
	}
	gotoPage(e){
		let newPageNo = e.target.value;
		this.setState({
			currPageNo: newPageNo
		});
		if(newPageNo != 1){
			this.setState({
				showPrevious: true
			});
		}else{
			this.setState({
				showPrevious: false
			});
		}
		if(newPageNo == this.state.totalPages){
			this.setState({
				showNext: false
			});
		}else{
			this.setState({
				showNext: true
			});
		}
		let searchStr = this.state.srcStr;
		searchStr = searchStr.trim();
		if(searchStr != "" && searchStr.length > 2){
			this.callGetEmployee(newPageNo, searchStr);
		}else{
			this.callGetEmployee(newPageNo, "");
		}
	}
	handleFirst(){
		let newPageNo = 1;
		this.setState({
			showPrevious: false,
			currPageNo: newPageNo
		});
		if(newPageNo == this.state.totalPages){
			this.setState({
				showNext: false
			});
		}else{
			this.setState({
				showNext: true
			});
		}
		let searchStr = this.state.srcStr;
		searchStr = searchStr.trim();
		if(searchStr != "" && searchStr.length > 2){
			this.callGetEmployee(newPageNo, searchStr);
		}else{
			this.callGetEmployee(newPageNo, "");
		}
	}
	handleLast(){
		let newPageNo = this.state.totalPages;
		if(newPageNo != 1){
			this.setState({
				showPrevious: true
			});
		}else{
			this.setState({
				showPrevious: false
			});
		}
		this.setState({
			showNext: false,
			currPageNo: newPageNo
		});
		let searchStr = this.state.srcStr;
		searchStr = searchStr.trim();
		if(searchStr != "" && searchStr.length > 2){
			this.callGetEmployee(newPageNo, searchStr);
		}else{
			this.callGetEmployee(newPageNo, "");
		}
	}
	handleNext(){
		let currPageNo = this.state.currPageNo;
		if(currPageNo < this.state.totalPages){
			let newPageNo = currPageNo + 1;
			if(newPageNo != 1){
				this.setState({
					showPrevious: true
				});
			}else{
				this.setState({
					showPrevious: false
				});
			}
			if(newPageNo == this.state.totalPages){
				this.setState({
					showNext: false
				});
			}else{
				this.setState({
					showNext: true
				});
			}
			this.setState({
				currPageNo: newPageNo
			});
			let searchStr = this.state.srcStr;
			searchStr = searchStr.trim();
			if(searchStr != "" && searchStr.length > 2){
				this.callGetEmployee(newPageNo, searchStr);
			}else{
				this.callGetEmployee(newPageNo, "");
			}
		}
	}
	handlePrevious(){
		let currPageNo = this.state.currPageNo;
		if(currPageNo != 1){
			let newPageNo = currPageNo - 1;
			if(newPageNo != 1){
				this.setState({
					showPrevious: true
				});
			}else{
				this.setState({
					showPrevious: false
				});
			}
			if(newPageNo == this.state.totalPages){
				this.setState({
					showNext: false
				});
			}else{
				this.setState({
					showNext: true
				});
			}
			this.setState({
				currPageNo: newPageNo
			});
			let searchStr = this.state.srcStr;
			searchStr = searchStr.trim();
			if(searchStr != "" && searchStr.length > 2){
				this.callGetEmployee(newPageNo, searchStr);
			}else{
				this.callGetEmployee(newPageNo, "");
			}
		}
	}
	componentDidMount(){
		this.callGetEmployee(1, "");
  	}
  render() {
  	const { emp } = this.state;
  	const { failureResponse } = this.state;
  	const { allPageNo } = this.state;

    return (
      <React.Fragment>
        <section className="content-header">
				<ol className="breadcrumb">
					<li><Link to="/">Home</Link></li>
					<li className="active">Employee Management</li>
				</ol>
		</section>
        <section className="content">
            <div className="addnew-parameter">
                <div className="upt">
                <input type="file" onChange={this.handleSubmit} ref={this.fileInput} name="importEmployees" id="file-1" className="inputfile inputfile-1 " /> 
                  <label htmlFor="file-1"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Import Employees</span></label>
            	</div>
            	<a href={Api.baseurl+'public/Employee.xlsx'} class="btn btn-default fix-button"><i class="fa fa-download" aria-hidden="true"></i> &nbsp;&nbsp;Download CSV</a>
            </div>
            <div className="row">
        	<div className="col-xs-12">
        	{
        		(this.state.messageView && failureResponse.length > 0)?
        		<p>
        			Some records failed with errors. Employee Code, First Name, Last Name and Company Name are mandatory.
        		</p>
        		:
        		(this.state.messageView)?<p>Employees imported/updated successfully.</p>:null
        	}
        		{
        			this.state.errMsg != ""?
        			<p>
        				{this.state.errMsg}
        			</p>
        		:
        		null
        		}

        		{
        			<p>{this.state.loaderText}</p>
        		}
        	</div>
        </div>
        <div className="row">
        	<div className="col-xs-4">
        		<input type="text" className="form-control" placeholder="Search Employee" onChange={this.handleChangeSrcStr} onKeyDown={this.searchEmployee} />
        	</div>
        </div>
        <div className="row">
					<div className="col-xs-12">
						<div className="box">
							
							<div className="box-body">
								<h4 className="table-heading">Employee Management</h4>
								<table id="example1 1" className="table table-bordered table-striped table-responsive">
									<thead>
										<tr>
											<th>Employee Code</th>
											<th>Employee Name</th>
											<th>Role</th>
											<th>Company</th>
											<th>Department</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
											{
												emp.map(hit => 
													<tr key={hit.id}>
														<td>{hit.employeeCode}</td>
														<td>{hit.employeeName}</td>
														<td>{hit.roleName}</td>
														<td>{hit.companyName}</td>
														<td>{hit.department}</td>
														<td><Link to={Api.baseurl+"Editemployee/"+hit.id}><img src={Api.baseurl+'public/images/edit-bg.png'} alt="edit"/></Link></td>
							                        </tr>
												)
											}
									</tbody>
									 
								</table>
								<p style={{textAlign:'right', marginTop:'10px'}}>
								{
									this.state.showPrevious == true && 
									<button className="btn" onClick={this.handleFirst}>First</button>
								}
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								{
									this.state.showPrevious == true && 
									<button className="btn" onClick={this.handlePrevious}>Previous</button>
								}
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

								<select name="pageNo" onChange={(e) => this.gotoPage(e)}>
									<option value="">-- Page No. --</option>
									{
										allPageNo.map(pagesHit => 
											<option value={pagesHit.value}>{pagesHit.value}</option>
										)
									}
								</select>

								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								{
									this.state.showNext == true && 
									<button className="btn" onClick={this.handleNext}>Next</button>
								}
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								{
									this.state.showNext == true && 
									<button className="btn" onClick={this.handleLast}>Last</button>
								}
								</p>
							</div>
							
						</div>
						
					</div>
				
				</div>
        </section>  
      </React.Fragment>
    )
  }
}

export default home;