import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import LogDisplay from './logdisplay';
import api from '../../Api.js';

class logtime extends Component {
	constructor(props){
		super(props);
		this.handleSave = this.handleSave.bind(this);
		this.callGetProjects = this.callGetProjects.bind(this);
		this.handleEstimateHours = this.handleEstimateHours.bind(this);
		this.handleHourSpent = this.handleHourSpent.bind(this);

		this.state = {
			apidata:[],
			projects: [],
          	selectedRole: "",
          	selectedRoleName: "",
          	errMsgHrSpent: "",
          	errMsgEstimateHours: "",
          	errMsgComment: "",
          	estimatedHours: "",
          	hourSpent: "",
          	isValid: false,
          	apiMsg: ""
        };
        this.projectId = React.createRef();
        this.roleId = React.createRef();
        this.date = React.createRef();
        this.estimatedHours = React.createRef();
        this.hoursSpent = React.createRef();
        this.comment = React.createRef();
	}
	handleEstimateHours(e){
		let val = e.target.value;
		if(val > 0 && val != "-0"){
			this.setState({
				estimatedHours: val
			});
		}else{
			this.setState({
				estimatedHours: ""
			});
		}
	}
	handleHourSpent(e){
		let val = e.target.value;
		if(val > 0 && val != "-0"){
			this.setState({
				hourSpent: val
			});
		}else{
			this.setState({
				hourSpent: ""
			});
		}
	}
	handleSave(){
		if(this.estimatedHours.current.value == ""){
			this.setState({
				errMsgEstimateHours: "Estimated hours cannot be blank.",
				isValid: false
			});
		}else{
			this.setState({
				errMsgEstimateHours: "",
				isValid: true
			});
		}
		if(this.hoursSpent.current.value == ""){
			this.setState({
				errMsgHrSpent: "Hours spent cannot be blank.",
				isValid: false
			});
		}else{
			this.setState({
				errMsgHrSpent: "",
				isValid: true
			});
		}
		if(this.comment.current.value == ""){
			this.setState({
				errMsgComment: "Comment cannot be blank.",
				isValid: false
			});
		}else{
			this.setState({
				errMsgComment: "",
				isValid: true
			});
		}
		if(this.state.isValid == true){
			this.setState({
				errMsgEstimateHours: "",
				errMsgHrSpent: "",
				errMsgComment: ""
			});
			let d = new Date();
			let mnth = d.getMonth() + 1;
			if(mnth < 9){
				mnth = "0"+mnth;
			}
			let dt = d.getDate();
			if(dt < 9){
				dt = "0"+dt;
			}
			let currDt = d.getFullYear()+"-"+mnth+"-"+dt;

			let employeeId = document.getElementById("employeeId").value;
			let Data = {"employeeId": employeeId, "projectId": this.projectId.current.value, "roleId": this.roleId.current.value, "date": currDt, "hoursSpent": this.hoursSpent.current.value, "comment": this.comment.current.value};
			let URL = api.endpoint+'saveLogEntries';
			fetch(URL, {
	          method: 'post',
	          body: JSON.stringify(Data)
	        })
	        .then(response => response.json())
	        .then(data => {
	        	this.setState({
	        		apiMsg: data.result
	        	});
	        	setTimeout(function(){
	        		location.reload();
	        	}, 2000);
	        });
		}
	}
	callGetProjects(){
		let employeeId = document.getElementById("employeeId").value;
		let Data = {"employeeId": employeeId};
		let URL = api.endpoint+'getEmployeeProjects';
		fetch(URL, {
          method: 'post',
          body: JSON.stringify(Data)
        })
        .then(response => response.json())
        .then(data => {
        	this.setState({
        		projects: data.projects
        	});
        });
	}
	componentWillMount(){
		var today = new Date();
		var dd= today.getDate();
		var mm= today.getMonth();
		var yy= today.getFullYear();
		if(dd<10){
			dd="0"+dd;
		}
		if(mm<10){
			mm="0"+mm;
		}
		mm++;
		if(mm <10){
			mm = "0"+mm;
		}
		var dateapi=yy+"-"+mm+"-"+dd;
		(async () => {
			const rawResponse = await fetch(api.endpoint+'getLogEntries', {
				method: 'POST',
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({ "employeeId": 1, "date":dateapi })
			});
			const content = await rawResponse.json();
			console.log(content.logEntry);
			this.setState({
				apidata:content.logEntry
			})
			
		})();

	}
	componentDidMount(){
		let employeeId = document.getElementById("employeeId").value;
		let Data = {"employeeId": employeeId};
        let URL = api.endpoint+'getEmployeeById';
        fetch(URL, {
          method: 'post',
          body: JSON.stringify(Data)
        })
        .then(response => response.json())
        .then(data => {
            this.setState({
                selectedRole: data.employees[0].roleId,
                selectedRoleName: data.employees[0].roleName
            });
            this.callGetProjects();
        });
	}
  render() {
  	const {projects} = this.state;
    return (
      <React.Fragment>
        <section className="content-header">
            <ol className="breadcrumb">
             <li><Link to={api.domain}>Home</Link></li>
             
             <li className="active">Log Time </li>
            </ol>
        </section>
        <section className="content">
            <div className="row">
                <div className="col-xs-12">
                    <div className="box">
                        <h4 class="table-heading">Log Time</h4>
													<div className="logtime-top">
															<form>
															<div className="row">
																	<div className="col-sm-3">
																		<div className="form-group">
																				<label>Project Name </label> 
																				<select name="project_state" className="form-control" ref={this.projectId}>
																					{
																						projects.map(hit => 
																							<option value={hit.projectId}>{hit.projectName}</option>
																						)
																					}
																				</select>
																		</div>
																	</div>
																<div className="col-sm-3">
																	<div className="form-group">
																		<label>Role</label> 
																		<select name="project_state" class="form-control" disabled="disabled" ref={this.roleId}>
																		<option value={this.state.selectedRole}>{this.state.selectedRoleName}</option>
																		</select>
																	</div>
																</div>
																<div className="col-sm-3">
																	<div className="form-group">
																	<label>Estimated Hours</label> 
																	<input type="number" value={this.state.estimatedHours} onChange={(e) => this.handleEstimateHours(e)} className="form-control" name="estimated_hrs" ref={this.estimatedHours}/>
																	<span style={{color: "red"}}>{this.state.errMsgEstimateHours}</span>
																	</div>
																</div>
																<div className="col-sm-3">
																	<div className="form-group">
																	<label>Hour Spent</label>
																	<input type="number" className="form-control" onChange={(e) => this.handleHourSpent(e)} value={this.state.hourSpent} name="hoursSpent" ref={this.hoursSpent}/> 
																	<span style={{color: "red"}}>{this.state.errMsgHrSpent}</span>
																	</div>
																</div>
															</div>
															<div className="row">
																<div className="col-sm-12">
																<label for="comment">Comments</label>
																<textarea className="form-control" rows="5" name="comments" ref={this.comment}></textarea>
																<span style={{color: "red"}}>{this.state.errMsgComment}</span>
																</div>
															</div>
															<div className="mgtop"></div>
																<div className="log-addbtn">
																<button type="button" className="btn btn-default fix-button" onClick={this.handleSave}>Add</button>
																<p>{this.state.apiMsg}</p>
															</div>
															</form>
													</div>
													<LogDisplay apidata={this.state.apidata}/>

                    </div>
                </div>
            </div>
        </section>
      </React.Fragment>
    )
  }
}
export default logtime
