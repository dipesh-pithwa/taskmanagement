import React,{Component} from 'react';
import {Link} from 'react-router-dom';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import '../../../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import Api from '../../Api';



class table extends Component {
  constructor(props){
    super(props);
    console.log("props Data: ");
    console.log(props.data);
    this.state = {
      projectData: props.data
    };
    this.getAccess = this.getAccess.bind(this);
  }

  getAccess(projectId){
    let userId = document.getElementById("userId").value;
    let Data = {"projectId":projectId,"userId":userId,"role":"Project Manager"};
    fetch(Api.endpoint+"getProjectAccess", {
      method: 'POST',
      headers: {
       'Accept': 'application/json',
       'Content-Type': 'application/json'
      },
      body: JSON.stringify(Data)
    })
    .then(res=>res.json())
    .then(function(data){
      alert(data.message);
    });
  }

    priceFormatter(cell, row){   
        return `<Link to="#"> ${cell}</Link>`;
      }

      getidgetdetail(id){
        let this2 = this;
        fetch(Api.endpoint+'getProjectDetails', {
             method: 'POST',
             headers: {
               'Accept': 'application/json',
               'Content-Type': 'application/json'
             },
             body: JSON.stringify({"projectId": id} )
           }).then(res=>res.json()).then(function(data){
               localStorage.setItem('getproject',JSON.stringify(data));
           });
      }
  
    render(){
      // console.log(this.state.projectData);
      const {projectData} = this.state;
        var products = [{
            id: 'HDFC',
            name: "SBI/123/7766",
            price: 'Get Access'
        }, {
            id: 'SBI',
            name: "Product2",
            price: 'view'
        }];
        return(
            <div className="prmgt">
            
                        <div className="box-body">
                            <h4>Project Management</h4>
                            <table id="example1" className="table table-bordered table-striped table-responsive">
                                            <thead>
                                                <tr>
                                                    <th>Project Name</th>
                                                    <th>Project Code</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            {
                                              projectData.map(hitProjects => {
                                                return(
                                                <tr>
                                                  <td>{hitProjects.projectName}</td>
                                                  <td>{hitProjects.projectCode}</td>
                                                  <td>
                                                    {
                                                      hitProjects.employeeAccess == "Y"?
                                                      <Link to={`Projectmngtab/${hitProjects.projectId}`}  onClick={this.getidgetdetail(hitProjects.projectId)}>View</Link>
                                                      :
                                                      <a href="javascript:void(0);" onClick={(e)=>this.getAccess(hitProjects.projectId)}>Get Access</a>
                                                    }
                                                  </td>
                                                </tr>
                                                )
                                              })
                                            }
                                            </tbody>
                                        </table>
    
                        </div>
            </div>
        )
    }
   
}

export default table;

