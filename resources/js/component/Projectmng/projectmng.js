import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import Addproject from './addprojectmodel';
import Api from '../../Api';

class projectmng extends Component {
   constructor(props){
     super(props);
     this.state={
        formsearchval:'null',
        projects:[],
        show:false,
        projectAccess: [],
        currPageNo: 1,
        totalPages: 0,
        showNext: true,
        showPrevious: false,
        allPageNo: [],
     }
    this.formsearch = this.formsearch.bind(this);
    this.getdetail = this.getdetail.bind(this);
    this.handleshow = this.handleshow.bind(this);
    this.handlehide = this.handlehide.bind(this);
    this.getAccess = this.getAccess.bind(this);
    this.getdetail = this.getdetail.bind(this);

    this.handleNext = this.handleNext.bind(this);
    this.handlePrevious = this.handlePrevious.bind(this);
    this.handleFirst = this.handleFirst.bind(this);
    this.handleLast = this.handleLast.bind(this);
    this.gotoPage = this.gotoPage.bind(this);
   }

   gotoPage(e){
    let newPageNo = e.target.value;
    this.setState({
      currPageNo: newPageNo
    });
    if(newPageNo != 1){
      this.setState({
        showPrevious: true
      });
    }else{
      this.setState({
        showPrevious: false
      });
    }
    if(newPageNo == this.state.totalPages){
      this.setState({
        showNext: false
      });
    }else{
      this.setState({
        showNext: true
      });
    }
    let searchStr = this.state.formsearchval;
    searchStr = searchStr.trim();
    if(searchStr != "" && searchStr.length > 2){
      this.getdetail(newPageNo, searchStr);
    }else{
      this.getdetail(newPageNo, "");
    }
  }
  handleFirst(){
    let newPageNo = 1;
    this.setState({
      showPrevious: false,
      currPageNo: newPageNo
    });
    if(newPageNo == this.state.totalPages){
      this.setState({
        showNext: false
      });
    }else{
      this.setState({
        showNext: true
      });
    }
    let searchStr = this.state.formsearchval;
    searchStr = searchStr.trim();
    if(searchStr != "" && searchStr.length > 2){
      this.getdetail(newPageNo, searchStr);
    }else{
      this.getdetail(newPageNo, "");
    }
  }
  handleLast(){
    let newPageNo = this.state.totalPages;
    if(newPageNo != 1){
      this.setState({
        showPrevious: true
      });
    }else{
      this.setState({
        showPrevious: false
      });
    }
    this.setState({
      showNext: false,
      currPageNo: newPageNo
    });
    let searchStr = this.state.formsearchval;
    searchStr = searchStr.trim();
    if(searchStr != "" && searchStr.length > 2){
      this.getdetail(newPageNo, searchStr);
    }else{
      this.getdetail(newPageNo, "");
    }
  }
  handleNext(){
    let currPageNo = this.state.currPageNo;
    if(currPageNo < this.state.totalPages){
      let newPageNo = currPageNo + 1;
      if(newPageNo != 1){
        this.setState({
          showPrevious: true
        });
      }else{
        this.setState({
          showPrevious: false
        });
      }
      if(newPageNo == this.state.totalPages){
        this.setState({
          showNext: false
        });
      }else{
        this.setState({
          showNext: true
        });
      }
      this.setState({
        currPageNo: newPageNo
      });
      let searchStr = this.state.formsearchval;
      searchStr = searchStr.trim();
      if(searchStr != "" && searchStr.length > 2){
        this.getdetail(newPageNo, searchStr);
      }else{
        this.getdetail(newPageNo, "");
      }
    }
  }
  handlePrevious(){
    let currPageNo = this.state.currPageNo;
    if(currPageNo != 1){
      let newPageNo = currPageNo - 1;
      if(newPageNo != 1){
        this.setState({
          showPrevious: true
        });
      }else{
        this.setState({
          showPrevious: false
        });
      }
      if(newPageNo == this.state.totalPages){
        this.setState({
          showNext: false
        });
      }else{
        this.setState({
          showNext: true
        });
      }
      this.setState({
        currPageNo: newPageNo
      });
      let searchStr = this.state.formsearchval;
      searchStr = searchStr.trim();
      if(searchStr != "" && searchStr.length > 2){
        this.getdetail(newPageNo, searchStr);
      }else{
        this.getdetail(newPageNo, "");
      }
    }
  }

   componentDidMount(){
    this.getdetail();
   }

   formsearch(e){
    this.setState({
      formsearchval:e.target.value
    })
   }

   getdetail(pageNo, searchStr){
     if(pageNo == "" || pageNo == null || pageNo === undefined){
       pageNo = 1;
     }
    let statedata=this.state.formsearchval=='null'?"":this.state.formsearchval;
    statedata = statedata.trim();
    if(statedata.length < 3 && statedata != ""){
      this.refs.errorform.innerHTML="Please Enter Project Name";
      return false;
    }
    else{
      this.refs.errorform.innerHTML="";
      (async () => {
        const rawResponse = await fetch(Api.endpoint+'searchProject', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({"projectName":statedata,"pageSize":'10',"pageNumber":pageNo} )
        });
        const content = await rawResponse.json();

        this.setState({
          totalPages: content.totalPages
        });

        let pageArr = new Array();
        for(let i = 1; i <= content.totalPages; i++){
          pageArr[i - 1] = {value: i};
        }
        this.setState({
          allPageNo: pageArr
        });

        if(content.totalPages == 1 || pageNo == content.totalPages || content.totalPages == 0){
          this.setState({showNext: false});
        }else if(content.totalPages > 1){
          this.setState({showNext: true});
        }

        let employeeId = document.getElementById("employeeId").value;
        let this2 = this;
        fetch(Api.endpoint+'getEmployeeProjects', {
          method: 'POST',
          headers: {
           'Accept': 'application/json',
           'Content-Type': 'application/json'
          },
          body: JSON.stringify({"employeeId": employeeId} )
        }).then(res=>res.json()).then(function(data){
          content.projects.map((curr, index)=>{
            content.projects[index].employeeAccess = "N";
            data.projects.map((curr2, index2) => {
              if(curr.projectId == curr2.projectId && curr2.roleName == "Project Manager"){
                console.log("IN Access IF.");
                content.projects[index].employeeAccess = "Y";
              }
            });
          });
          this2.setState({
            projects:content.projects
          });
        });
      })();
    }

   }

   handleshow(){
    this.setState({
        show:true,
        addclass:'in'
    })
}
handlehide(){
    this.setState({
        show:false,
        addclass:''
    })
}

getAccess(projectId){
    let userId = document.getElementById("userId").value;
    let Data = {"projectId":projectId,"userId":userId,"role":"Project Manager"};
    fetch(Api.endpoint+"getProjectAccess", {
      method: 'POST',
      headers: {
       'Accept': 'application/json',
       'Content-Type': 'application/json'
      },
      body: JSON.stringify(Data)
    })
    .then(res=>res.json())
    .then(function(data){
      alert(data.message);
    });
  }

      getidgetdetail(id){
        let this2 = this;
        fetch(Api.endpoint+'getProjectDetails', {
             method: 'POST',
             headers: {
               'Accept': 'application/json',
               'Content-Type': 'application/json'
             },
             body: JSON.stringify({"projectId": id} )
           }).then(res=>res.json()).then(function(data){
               localStorage.setItem('getproject',JSON.stringify(data));
           });
      }

  
  render() {
   const {projects} = this.state;
        var products = [{
            id: 'HDFC',
            name: "SBI/123/7766",
            price: 'Get Access'
        }, {
            id: 'SBI',
            name: "Product2",
            price: 'view'
        }];
        const { allPageNo } = this.state;
    return (
      <React.Fragment>
        <section className="content-header">
            <ol className="breadcrumb">
            <li><Link to="/">Home</Link></li>
            <li className="active">Project Management</li>
            </ol>
        </section>
        <section className="content">
            <div className="row">
                <div className="col-xs-12">
                  <div className="col-md-6">
                  <div className="pr-addbtn">
                    <button type="button" className="btn btn-default fix-button" data-toggle="modal" data-target=".bs-example-modal-lg" onClick={(e)=>this.handleshow(e)}>Add Project</button>
                  </div>
                  </div>
                  <div className="col-md-6">
                    <div className="navbar-form navbar-left" role="search">
                    <div className="form-group">
                      <input type="text" ref="searchform" className="form-control" onChange={this.formsearch} name="searchform" placeholder="Search..." style={{"border":'1px solid ##337ab7'}}/>
                    </div>
                      <button type="button" className="btn btn-default" onClick={(e)=>this.getdetail()}>Get Detail</button>
                    </div>
                    <span ref="errorform" style={{color:'red'}} className="col-xs-12"></span>
                  </div>
                  <div style={{clear:'both'}}></div>
                    <div className="prmgt">
            
                        <div className="box-body">
                            <h4>Project Management</h4>
                            <table id="example1" className="table table-bordered table-striped table-responsive">
                                            <thead>
                                                <tr>
                                                    <th>Project Name</th>
                                                    <th>Project Code</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            {
                                              projects.map(hitProjects => {
                                                return(
                                                <tr>
                                                  <td>{hitProjects.projectName}</td>
                                                  <td>{hitProjects.projectCode}</td>
                                                  <td>
                                                    {
                                                      hitProjects.employeeAccess == "Y"?
                                                      <Link to={`Projectmngtab/${hitProjects.projectId}`}  onClick={this.getidgetdetail(hitProjects.projectId)}>View</Link>
                                                      :
                                                      <a href="javascript:void(0);" onClick={(e)=>this.getAccess(hitProjects.projectId)}>Get Access</a>
                                                    }
                                                  </td>
                                                </tr>
                                                )
                                              })
                                            }
                                            </tbody>
                                        </table>
                                        <p style={{textAlign:'right', marginTop:'10px'}}>
                                        {
                                          this.state.showPrevious == true && 
                                          <button className="btn" onClick={this.handleFirst}>First</button>
                                        }
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        {
                                          this.state.showPrevious == true && 
                                          <button className="btn" onClick={this.handlePrevious}>Previous</button>
                                        }
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                        <select name="pageNo" onChange={(e) => this.gotoPage(e)}>
                                          <option value="">-- Page No. --</option>
                                          {
                                            allPageNo.map(pagesHit => 
                                              <option value={pagesHit.value}>{pagesHit.value}</option>
                                            )
                                          }
                                        </select>

                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        {
                                          this.state.showNext == true && 
                                          <button className="btn" onClick={this.handleNext}>Next</button>
                                        }
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        {
                                          this.state.showNext == true && 
                                          <button className="btn" onClick={this.handleLast}>Last</button>
                                        }
                                        </p>
    
                        </div>
            </div>
                 
                  
                 
                </div>
                {/* end col-xs12 */}
            </div>
            {this.state.show? <Addproject closemodel={this.handlehide}/>:null}
        </section>
      </React.Fragment>
    )
  }
}

export default projectmng;