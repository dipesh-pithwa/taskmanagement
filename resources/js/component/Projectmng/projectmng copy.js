import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import Tableview from './table';
import Addproject from './addprojectmodel';
import Api from '../../Api';

class projectmng extends Component {
   constructor(props){
     super(props);
     this.state={
        formsearchval:'null',
        projects:[],
        show:false,
        projectAccess: [],
     }
     this.formsearch = this.formsearch.bind(this);
     this.getdetail = this.getdetail.bind(this);
     this.handleshow = this.handleshow.bind(this);
     this.handlehide = this.handlehide.bind(this);
   }

   componentDidMount(){
    this.getdetail();
   }

   formsearch(e){
    this.setState({
      formsearchval:e.target.value
    })
   }

   getdetail(e){
    let statedata=this.state.formsearchval=='null'?"":this.state.formsearchval;
    if(statedata.length < 3 && statedata != ""){
      this.refs.errorform.innerHTML="Please Enter Project Name";
      return false;
    }
    else{
      this.refs.errorform.innerHTML="";
      (async () => {
        const rawResponse = await fetch(Api.endpoint+'searchProject', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({"projectName":statedata,"pageSize":'20',"pageNumber":"1"} )
        });
        const content = await rawResponse.json();


        let employeeId = document.getElementById("employeeId").value;
        let this2 = this;
        fetch(Api.endpoint+'getEmployeeProjects', {
          method: 'POST',
          headers: {
           'Accept': 'application/json',
           'Content-Type': 'application/json'
          },
          body: JSON.stringify({"employeeId": employeeId} )
        }).then(res=>res.json()).then(function(data){
          content.projects.map((curr, index)=>{
            content.projects[index].employeeAccess = "N";
            data.projects.map((curr2, index2) => {
              if(curr.projectId == curr2.projectId && curr2.roleName == "Project Manager"){
                console.log("IN Access IF.");
                content.projects[index].employeeAccess = "Y";
              }
            });
          });
          this2.setState({
            projects:content.projects
          });
        });
      })();
    }

   }

   handleshow(){
    this.setState({
        show:true,
        addclass:'in'
    })
}
handlehide(){
    this.setState({
        show:false,
        addclass:''
    })
}

  
  render() {
   console.log(this.state.projects);
    return (
      <React.Fragment>
        <section className="content-header">
            <ol className="breadcrumb">
            <li><Link to="/">Home</Link></li>
            <li className="active">Project Management</li>
            </ol>
        </section>
        <section className="content">
            <div className="row">
                <div className="col-xs-12">
                  <div className="col-md-6">
                  <div className="pr-addbtn">
                    <button type="button" className="btn btn-default fix-button" data-toggle="modal" data-target=".bs-example-modal-lg" onClick={(e)=>this.handleshow(e)}>Add Project</button>
                  </div>
                  </div>
                  <div className="col-md-6">
                    <div className="navbar-form navbar-left" role="search">
                    <div className="form-group">
                      <input type="text" ref="searchform" className="form-control" onChange={this.formsearch} name="searchform" placeholder="Search..." style={{"border":'1px solid ##337ab7'}}/>
                    </div>
                      <button type="button" className="btn btn-default" onClick={(e)=>this.getdetail(e)}>Get Detail</button>
                    </div>
                    <span ref="errorform" style={{color:'red'}} className="col-xs-12"></span>
                  </div>
                  <div style={{clear:'both'}}></div>
                    {this.state.projects.length!==0? <Tableview viewpart={this.veiwrender} data={this.state.projects}/>:<div>Please search ...</div>}
                 
                  
                 
                </div>
                {/* end col-xs12 */}
            </div>
            {this.state.show? <Addproject closemodel={this.handlehide}/>:null}
        </section>
      </React.Fragment>
    )
  }
}

export default projectmng;