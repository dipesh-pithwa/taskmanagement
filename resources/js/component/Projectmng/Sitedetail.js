import React, { Component } from 'react';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Api from '../../Api';

export default class Sitedetail extends Component {
  constructor(props){
    super(props)
    this.state={
        siteform:{"projectId":"","site":{"id":"","siteName":"","addressLine1":"","addressLine2":"","city":"","state":"","country":"","zipCode":""}},
        msgError:null,
        msg:null,
        errors:{},

    }
    this.siteonchange = this.siteonchange.bind(this);
    this.savesitedata = this.savesitedata.bind(this);
    this.validationSite = this.validationSite.bind(this);

  }

  componentWillMount(){
    var geturl =window.location.href.split('/');
    let getnumber=geturl.length-1;
    

    var getdata = localStorage.getItem("getproject");
    var strdata = JSON.parse(getdata);
    let site=strdata.site;

    this.setState({siteform:({projectId:geturl[getnumber],site:({...site,"siteName":'',"id":''})})})
    //console.log(strdata);
    
  }

  
  siteonchange(feildname,e){
    console.log(feildname);
    let site= this.state.siteform.site;
    let getstateid= this.state.siteform.projectId;

    if(feildname == "zipCode"){
      if(e.target.value > 0 && e.target.value != "-0"){
        site[feildname]=e.target.value;
      }else{
        site[feildname]="";
      }
    }else{
      site[feildname]=e.target.value;
    }
    
    this.setState({siteform:({projectId:getstateid,site:({...site,"siteName":'',"id":'1'})})})

  }

  validationSite(){
    let getsatedata=this.state.siteform.site;
    let Ischeckvalid=true;
    let errors={}
    if (getsatedata.addressLine1 === ""||getsatedata.addressLine1 === null) {
      Ischeckvalid=false;
      errors['addressLine1']="Enter a Address";
    }else{
      errors['addressLine1']="";
    }
   
    if (getsatedata.addressLine2 === "" || getsatedata.addressLine2 === null) {
      Ischeckvalid=false;
      errors['addressLine2']="Enter a Address";
    }else{
      errors['addressLine2']="";
    }
  
    if (getsatedata.city === ""||getsatedata.city===null) {
      Ischeckvalid=false;
      errors['city']="city is mandatory";
    }else{
      errors['city']="";
    }
    if (!/^[a-zA-Z]*$/g.test(getsatedata.city)) {
      Ischeckvalid=false;
      errors['city']="Enter only character";
    }else{
      errors['city']="";
    }
    if(this.refs.state.value===""){
      Ischeckvalid=false;
      errors['state']="Please select State";
    }else{
      errors['state']="";
    }
    if (getsatedata.zipCode === ""||getsatedata.zipCode===null) {
      Ischeckvalid=false;
      errors['zipCode']="zipCode is mandatory";
    }else{
      errors['zipCode']="";
    }
    if (getsatedata.country === ""||getsatedata.country===null) {
      Ischeckvalid=false;
      errors['country']="country is mandatory";
    }else{
      errors['country']="";
    }
    if (!/^[a-zA-Z]*$/g.test(getsatedata.country)) {
      Ischeckvalid=false;
      errors['country']="Enter only character";
    }else{
      errors['country']="";
    }
    // if (getsatedata.area === ""||getsatedata.area===null) {
    //   Ischeckvalid=false;
    //   errors['area']="enter area";
    // }
    
    this.setState({
      errors:errors
    })
    return Ischeckvalid;

  }

  savesitedata(){
  
    let getsatedata=this.state.siteform;
    console.log(getsatedata);
    let valistate= this.state.siteform.site;
    let checkvalid = this.validationSite();

    if(checkvalid){
      
      (async () => {
        const rawResponse = await fetch(Api.endpoint+'saveSiteDetails', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(getsatedata)
        });
        const content = await rawResponse.json();
        this.setState({
          msg:content.message
        })
        setTimeout(function(){
          this.setState({
            msg:null
          })
        }.bind(this),5000)
       
      })();
    }
    
    
   



   }
  

  render() {
    var errorstyle = {
            color: 'red',
            position:'absolute',
            fontSize:'12px',
        };
    console.log(this.state.siteform)
    return (
        <div className="form-box-mgmt">
        
        <div className="row">
          <div className="col-sm-4 form-group">
            <label>Address Line 1</label>
            <input type="text" onChange={(e)=>this.siteonchange('addressLine1',e)}  className="form-control" name="addressLine1" ref="addressLine1" value={this.state.siteform.site.addressLine1||''} />
            <span style={errorstyle}>{this.state.errors["addressLine1"]}</span>
          </div>
          <div className="col-sm-4 form-group">
            <label>Address Line 2</label>
            <input type="text" className="form-control" onChange={(e)=>this.siteonchange('addressLine2',e)}  value={this.state.siteform.site.addressLine2||''} name="addressLine2" ref="addressLine2"/>
            <span style={errorstyle}>{this.state.errors["addressLine2"]}</span>
          </div>
          <div className="col-sm-4 form-group">
            <label>City</label>
            <input type="text" className="form-control" onChange={(e)=>this.siteonchange('city',e)}  value={this.state.siteform.site.city||''}  name="city" ref="city"/>
            <span style={errorstyle}>{this.state.errors["city"]}</span>
          </div>
          <div className="col-sm-4 form-group">
            <label>State</label>
            <select name="state" ref="state" className="form-control" onChange={(e)=>this.siteonchange('state',e)} >
            <option value="">--Select--</option>
              <option value="Andra Pradesh">Andra Pradesh</option>
              <option value="Arunachal Pradesh">Arunachal Pradesh</option>
              <option value="Assam">Assam</option>
              <option value="Bihar">Bihar</option>
              <option value="Chhattisgarh">Chhattisgarh</option>
              <option value="Goa">Goa</option>
              <option value="Gujarat">Gujarat</option>
              <option value="Haryana">Haryana</option>
              <option value="Himachal Pradesh">Himachal Pradesh</option>
              <option value="Jammu and Kashmir">Jammu and Kashmir</option>
              <option value="Jharkhand">Jharkhand</option>
              <option value="Karnataka">Karnataka</option>
              <option value="Kerala">Kerala</option>
              <option value="Madya Pradesh">Madya Pradesh</option>
              <option value="Maharashtra">Maharashtra</option>
              <option value="Manipur">Manipur</option>
              <option value="Meghalaya">Meghalaya</option>
              <option value="Mizoram">Mizoram</option>
              <option value="Nagaland">Nagaland</option>
              <option value="Orissa">Orissa</option>
              <option value="Punjab">Punjab</option>
              <option value="Rajasthan">Rajasthan</option>
              <option value="Sikkim">Sikkim</option>
              <option value="Tamil Nadu">Tamil Nadu</option>
              <option value="Telagana">Telagana</option>
              <option value="Tripura">Tripura</option>
              <option value="Uttaranchal">Uttaranchal</option>
              <option value="Uttar Pradesh">Uttar Pradesh</option>
              <option value="West Bengal">West Bengal</option>
              <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
              <option value="Chandigarh">Chandigarh</option>
              <option value="Dadar and Nagar Haveli">Dadar and Nagar Haveli</option>
              <option value="Daman and Diu">Daman and Diu</option>
              <option value="Delhi">Delhi</option>
              <option value="Lakshadeep">Lakshadeep</option>
              <option value="Pondicherry">Pondicherry</option>
            </select>
            <span style={errorstyle}>{this.state.errors["state"]}</span>
          </div>
          <div className="col-sm-4 form-group">
            <label>Zip Code</label>
            <input type="number" ref="zipCode" onChange={(e)=>this.siteonchange('zipCode',e)}  className="form-control" value={this.state.siteform.site.zipCode||''} name="zipCode"/>
            <span style={errorstyle}>{this.state.errors["zipCode"]}</span>
          </div>
          <div className="col-sm-4 form-group">
            <label>Country</label>
            <input type="text" value={this.state.siteform.site.country||''}  className="form-control" name="country" ref="country" onChange={(e)=>this.siteonchange('country',e)}/>
            <span style={errorstyle}>{this.state.errors["country"]}</span>
          </div>
          {/* <div className="col-sm-4 form-group">
            <label>Area (in Sq. Feet)</label>
            <input type="number" className="form-control"  onChange={(e)=>this.siteonchange('area',e)} name="area" ref="area" value={this.state.siteform.site.area||''}/>
            <span style={errorstyle}>{this.state.errors["area"]}</span>
          </div> */}
        </div>
        <div className="row mgtop">
          <div className="col-md-12">
            {this.state.msg!==null?<p>{this.state.msg}</p>:null}

            {this.state.msgError!==null?<p className="btn btn-danger">{this.state.msgError}</p>:null}
            
          </div>
          <div className="col-sm-12">
            <div className="form-group">
              <button type="button" className="btn btn-default fix-button" onClick={((e)=>this.savesitedata())} style={{marginRight:'8px'}}>Save</button>
              <button type="button" className="btn btn-default fix-button clr-new">Close</button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
