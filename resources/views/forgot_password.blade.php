<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Vector | Forgot Password</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <!-- Bootstrap 3.3.7 -->
      <link rel="stylesheet" href="/livepages/public/bower_components/bootstrap/dist/css/bootstrap.min.css">
      <!-- Font Awesome -->
      <link rel="stylesheet" href="/livepages/public/bower_components/font-awesome/css/font-awesome.min.css">
      <!-- Ionicons -->
      <link rel="stylesheet" href="/livepages/public/bower_components/Ionicons/css/ionicons.min.css">
      <!-- Theme style -->
      <link rel="stylesheet" href="/livepages/public/dist/css/AdminLTE.min.css">
      <!-- iCheck -->
      <!-- <link rel="stylesheet" href="/public/plugins/iCheck/square/blue.css"> -->
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
      <!-- Google Font -->
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
   </head>
   <body class="hold-transition login-page">
      <div class="login-box">
         <div class="login-logo">
            <a href="#"><b><img src="/livepages/public/images/logo.jpg"></b> </a>
         </div>
         <!-- /.login-logo -->
         <div class="login-box-body">
               <input type="hidden" id="encPass" value="1" />
               <input type="hidden" id="oldPassword" value="<?=$Password?>" />
               <input type="hidden" id="username" value="<?=$EmailId?>">
               <div class="form-group has-feedback">
                  <input type="password" class="form-control" placeholder="New Password" id="newPassword" name="newPassword">
                  <span class="glyphicon glyphicon-lock form-control-feedback"></span>
               </div>
               <div class="form-group has-feedback">
                  <input type="password" class="form-control" placeholder="Re Enter Password" id="reenterPassword" name="reenterPassword">
                  <span class="glyphicon glyphicon-lock form-control-feedback"></span>
               </div>
               <div class="row">
                  <div class="col-xs-8">
                     <p id="msg"></p>
                  </div>
                  <!-- /.col -->
                  <div class="col-xs-4">
                     <button id="btn_submit" class="btn btn-primary btn-block btn-flat btn-vector">Submit</button>
                  </div>
                  <!-- /.col -->
               </div>
            <!-- /.social-auth-links -->
         </div>
         <!-- /.login-box-body -->
      </div>
      <!-- /.login-box -->
      <!-- jQuery 3 -->
      <script src="/livepages/public/bower_components/jquery/dist/jquery.min.js"></script>
      <!-- Bootstrap 3.3.7 -->
      <script src="/livepages/public/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
      <!-- iCheck -->
      <!-- <script src="/public/plugins/iCheck/icheck.min.js"></script> -->
      <script src="/livepages/public/js/jquery.form.js"></script>
      <script>
         $(document).ready(function(){
            $("#btn_submit").click(function(){
               var format = /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
               var format2 = /[0-9]/g;
               var encPass = $("#encPass").val();
               var oldPassword = $("#oldPassword").val();
               var username = $("#username").val();
               var newPassword = $("#newPassword").val();
               var reenterPassword = $("#reenterPassword").val();
               if(newPassword == ""){
                  $("#msg").text("New Password cannot be blank.");
               }else if(newPassword.length < 8){
                  $("#msg").text("Minimum Password lenght should be 8 characters.");
               }else if(reenterPassword == ""){
                  $("#msg").text("Re-Enter Password cannot be blank.");
               }else if(newPassword != reenterPassword){
                  $("#msg").text("New Password and Re-Enter Password should be same.");
               }else if(!format.test(newPassword)){
                  $("#msg").text("Password must contain atleast 1 number and 1 special character.");
               }else if(!format2.test(newPassword)){
                  $("#msg").text("Password must contain atleast 1 number and 1 special character.");
               }else{
                  $.ajax({
                     url: "http://localhost/livepages/index.php/api/changePassword",
                     type: "post",
                     "headers": {
                        "Content-Type": "application/json",
                     },
                     data: "{\"encPass\": \""+encPass+"\", \"oldPassword\": \""+oldPassword+"\", \"username\": \""+username+"\", \"newPassword\": \""+newPassword+"\", \"reenterPassword\": \""+reenterPassword+"\"}",
                     success: function(response){
                        console.log(response);
                        var resp = JSON.parse(response);
                        if(resp.result == "success"){
                           $("#msg").text(resp.message);
                        }else{
                           $("#msg").text(resp.message);
                        }
                     }
                  });
               }
            });
         });
      </script>
   </body>
</html>