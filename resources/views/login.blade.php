<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Vector</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <!-- Bootstrap 3.3.7 -->
      <link rel="stylesheet" href="/livepages/public/bower_components/bootstrap/dist/css/bootstrap.min.css">
      <!-- Font Awesome -->
      <link rel="stylesheet" href="/livepages/public/bower_components/font-awesome/css/font-awesome.min.css">
      <!-- Ionicons -->
      <link rel="stylesheet" href="/livepages/public/bower_components/Ionicons/css/ionicons.min.css">
      <!-- Theme style -->
      <link rel="stylesheet" href="/livepages/public/dist/css/AdminLTE.min.css">
      <!-- iCheck -->
      <!-- <link rel="stylesheet" href="/public/plugins/iCheck/square/blue.css"> -->
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
      <!-- Google Font -->
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
   </head>
   <body class="hold-transition login-page">
      <div class="login-box">
         <div class="login-logo">
            <a href="#"><b><img src="/livepages/public/images/logo.jpg"></b> </a>
         </div>
         <!-- /.login-logo -->
         <div class="login-box-body">
            <form action="http://localhost/livepages/index.php/api/doLogin" method="post" id="frmLogin">
               <!-- <input type="hidden" name="_token" value="<?php echo csrf_token();?>"> -->
               <div class="form-group has-feedback">
                  <input type="email" class="form-control" placeholder="Email" id="txtEmailId" name="txtEmailId">
                  <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
               </div>
               <div class="form-group has-feedback">
                  <input type="password" class="form-control" placeholder="Password" id="txtPassword" name="txtPassword">
                  <span class="glyphicon glyphicon-lock form-control-feedback"></span>
               </div>
               <div class="row">
                  <div class="col-xs-8">
                     <div class="checkbox icheck">
                        <a href="javascript:void(0);" id="forgotpswd">Forgot Password ?</a>
                        <p id="msg"></p>
                     </div>
                  </div>
                  <!-- /.col -->
                  <div class="col-xs-4">
                     <button id="frmsubmit" class="btn btn-primary btn-block btn-flat btn-vector">Sign In</button>
                  </div>
                  <!-- /.col -->
               </div>
            </form>
            <!-- /.social-auth-links -->
         </div>
         <!-- /.login-box-body -->
      </div>
      <!-- /.login-box -->
      <!-- jQuery 3 -->
      <script src="/livepages/public/bower_components/jquery/dist/jquery.min.js"></script>
      <!-- Bootstrap 3.3.7 -->
      <script src="/livepages/public/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
      <!-- iCheck -->
      <!-- <script src="/public/plugins/iCheck/icheck.min.js"></script> -->
      <script src="/livepages/public/js/jquery.form.js"></script>
      <script>
         $(function () {
            // $('input').iCheck({
            //  checkboxClass: 'icheckbox_square-blue',
            //  radioClass: 'iradio_square-blue',
            //  increaseArea: '20%' /* optional */
            // });

            $('#frmLogin').ajaxForm(function(response) { 
                var res = JSON.parse(response);

                if(res.status==1){
                  location.href = "/livepages/";
                }else{
                  $("#msg").text("Please provide valid user name and password.");
                }
            }); 

         });
         $(document).ready(function(){
            $("#frmsubmit").click(function(){
               var EmailId = $("#txtEmailId").val();
               var Pass = $("#txtPassword").val();
               if(EmailId == "" || Pass == ""){
                  $("#msg").text("Please provide valid user name and password.");
               }else{
                  $("#msg").text("");
                  $('#frmLogin').submit();
               }
            });
            $("#forgotpswd").click(function(){
               var EmailId = $("#txtEmailId").val();
               if(EmailId != ""){
                  $.ajax({
                     url: "http://localhost/livepages/index.php/api/forgotPassword",
                     type: "post",
                     "headers": {
                        "Content-Type": "application/json",
                     },
                     data: "{\"emailAddress\": \""+EmailId+"\"}",
                     success: function(response){
                        console.log(response);
                        var resp = JSON.parse(response);
                        if(resp.result == "success"){
                           $("#msg").text(resp.message);
                        }else{
                           $("#msg").text(resp.message);
                        }
                     }
                  });
               }else{
                  $("#msg").text("Please provide valid user name.");
               }
            });
         });
      </script>
   </body>
</html>