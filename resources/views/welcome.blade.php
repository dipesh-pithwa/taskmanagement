<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Task Management</title>
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
        <link rel="stylesheet" type="text/css" href="/livepages/public/dist/AdminLTE.min.css">
        <link rel="stylesheet" type="text/css" href="/livepages/public/dist/_all-skins.min.css">
    </head>
    <input type="hidden" id="emailId" value="<?=@$emailId?>" />
    <input type="hidden" id="firstName" value="<?=@$firstName?>" />
    <input type="hidden" id="role" value="<?=@$role?>" />
    <input type="hidden" id="tenantId" value="<?=@$tenantId?>" />
    <input type="hidden" id="employeeId" value="<?=@$employeeId?>" />
    <input type="hidden" id="secret" value="<?=@$secret?>" />
    <input type="hidden" id="userId" value="<?=@$userId?>" />
    <body class="hold-transition skin-blue sidebar-mini" style="height: auto;minHeight:100%;">
        <div id="root" class="wrapper"></div>
        <script src="{{mix('js/index.js')}}" ></script>
    </body>
</html>